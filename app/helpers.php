<?php

use App\Models\Course;
use App\Models\User;
use App\Models\SiteManagement;

use Illuminate\Http\Request;

if (!function_exists('getRoleCMS')) {
    function getRoleCMS()
    {
        $data = User::select('role')->get();
        return $data;
    }
}

if (!function_exists('getCourse')) {
    function getCourse()
    {
        $data = Course::where('status', 'Aktif')->get();
        return $data;
    }
}

if (!function_exists('getContact')) {
    function getContact()
    {
        $data = SiteManagement::where('keyname', 'contact')->first();
        
        if (!$data) {
            $data['value'] = [
                'address' => '',
                'phone' => '',
                'email' => '',
            ];

            $data['value'] = (object) $data['value'];
        } else {
            $data = ['value' => json_decode($data->value)];
        }
        
        return $data;
    }
}

if (!function_exists('uploadImg')) {
    function uploadImg($request, $field, $folder) {
        $image = $request->file($field);
        $nama_gambar = date('YmdHis').'-'.$image->getClientOriginalName();
        $size = $image->getSize();
        $photos = Image::make($image->path());
        if ($size > 500000) {
            $photos->resize(1000, 1000, function ($const) {
                $const->aspectRatio();
            })->save($folder.'/'.$nama_gambar);
        } else {
            $image->move($folder, $nama_gambar);
        }
        
        return $nama_gambar;
    }
}

if (!function_exists('uploadImgMultiple')) {
    function uploadImgMultiple($request, $field, $folder) {
        // $image = $request->file($field);
        $images = [];
        
        foreach($image = $request->file($field) as $key => $file){
            $nama_gambar = date('YmdHis').'-'.$file->getClientOriginalName();
            $size = $file->getSize();
            $photos = Image::make($file->path());
            if ($size > 500000) {
                $photos->resize(1000, 1000, function ($const) {
                    $const->aspectRatio();
                })->save($folder.'/'.$nama_gambar);
            } else {
                $file->move($folder, $nama_gambar);
            }
            $images[$key] = $nama_gambar;
            // array_push($images, $nama_gambar);
        }
        
        return $images;
    }
}

if (!function_exists('uploadPdf')) {
    function uploadPdf($request, $field, $folder) {
        $image = $request->file($field);
        $nama_file = date('YmdHis').'-'.$image->getClientOriginalName();
        $image->move($folder, $nama_file);

        return $nama_file;
    }
}

if (!function_exists('getGuardCms')) {
    function getGuardCms()
    {
        $role = getRoleCMS();

        $checkRole = false;
        foreach ($role as $value) {
            if (auth()->guard($value->role)->user()) {
                $checkRole = auth()->guard($value->role)->user();
                break;
            }
        }
        return $checkRole ? $checkRole : false;
    }
}

if (!function_exists('getGuardFe')) {
    function getGuardFe()
    {
        $data = auth()->guard('member')->user();
        return $data ? $data : false;
    }
}

if (!function_exists('strLimit')) {
    function strLimit($str, $len){
        if (strlen($str) > $len){
            $str = substr($str, 0, $len) . '...';
        }
        return $str; 
    }
}

if (!function_exists('tanggalIndo')) {
    function tanggalIndo($tgl){
        $pecahWaktu = explode(" ", $tgl);
        
        $getTgl = explode("-", $pecahWaktu[0]);
        $tgl    = $getTgl[2];
        $bulan  = bulanIndonesia($getTgl[1]);
        $tahun  = $getTgl[0];
        
        return $tgl ." ". $bulan ." ". $tahun; 
    }    
}

if (!function_exists('tanggalIndo2')) {
    function tanggalIndo2($tgl){
        $pecahWaktu = explode(" ", $tgl);
        
        $getTgl = explode("-", $pecahWaktu[0]);
        $tgl    = $getTgl[2];
        $bulan  = bulanIndonesia($getTgl[1]);
        $tahun  = $getTgl[0];
        
        return $tgl ." ". substr($bulan, 0, 3) ." ". $tahun; 
    }    
}

if (!function_exists('bulanIndonesia')) {
    function bulanIndonesia($bulan){
    
        $bulanArray = [
            '01'  => "Januari",
            '02'  => "Febuari",
            '03'  => "Maret",
            '04'  => "April",
            '05'  => "Mei",
            '06'  => "Juni",
            '07'  => "Juli",
            '08'  => "Agustus",
            '09'  => "September",
            '10' => "Oktober",
            '11' => "November",
            '12' => "Desember"
        ];
    
        $setBulan = $bulanArray[$bulan];  
    
        return $setBulan;
    }
}

if (!function_exists('rupiah')) {
    function rupiah($uang){
        $setRupiah = number_format($uang,0, ',', '.');
        return 'Rp '.$setRupiah.',-';
    }
}


