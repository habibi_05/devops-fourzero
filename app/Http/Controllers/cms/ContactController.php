<?php

namespace App\Http\Controllers\cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SiteManagement;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta = [
            'title'			=> 'Contact',
			'breadcrumb'	=> [
				'Dashboard' => "/",
				'Contact'	=> null
			]
        ];

        $plugins = [
            'js' => [
                'dashboard/plugins/sweetalert/sweetalert.min.js',
                'dashboard/plugins/sweetalert/jquery.sweet-alert.custom.js',
            ]
        ];

        $data = SiteManagement::where('keyname', 'contact')->first();

        if (!$data) {
            $data['value'] = [
                'address' => '',
                'phone' => '',
                'email' => '',
            ];

            $data['value'] = (object) $data['value'];
        } else {
            $data = ['value' => json_decode($data->value)];
        }

        return view('cms.contact.index', compact('meta', 'plugins', 'data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'address' => 'required',
            'phone' =>'required',
            'email' =>'required'
        ]);

        SiteManagement::updateOrCreate(
            ['section' => 'site', 'keyname' => 'contact'],
            [
                'value' => json_encode([
                    'address' => $request->address,
                    'phone' => $request->phone,
                    'email' => $request->email,
                ])
            ]
        );

        return redirect()->route('contact.index')->with('success','Data Contact Berhasil Disimpan');
    }
}
