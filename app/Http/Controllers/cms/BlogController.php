<?php

namespace App\Http\Controllers\cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\Tag;
use App\Models\KategoriBlog;
use DataTables;
use Image;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function dataJson(){
        return Datatables::of(Blog::all())
        ->addColumn('action', function ($row) {
            $action = '<a style="margin-right:2px;" href="'.url('blog/'.$row->id.'/edit').'" class="btn btn-inverse-primary"><i class="bx bx-edit me-0"></i></a>';
            $action .= '<button data-remote="'.url('blog/'. $row->id).'" class="btn btn-delete btn-inverse-danger"><i class="bx bx-trash me-0"></i></button>';
            
            return $action;
        })
        ->make(true);
    }

    public function index()
    {
        $meta = [
            'title'			=> 'Blog',
			'breadcrumb'	=> [
				'Dashboard' => "/",
				'Blog'	=> null
			]
        ];

        $plugins = [
            'css' => [
                'dashboard/plugins/datatable/css/dataTables.bootstrap5.min.css',
                'dashboard/plugins/notifications/css/lobibox.min.css'
            ],
            'js' => [
                'dashboard/plugins/datatable/js/jquery.dataTables.min.js',
                'dashboard/plugins/datatable/js/dataTables.bootstrap5.min.js',
                'dashboard/plugins/notifications/js/lobibox.min.js',
                'dashboard/plugins/sweetalert/sweetalert.min.js',
                'dashboard/plugins/sweetalert/jquery.sweet-alert.custom.js',
            ]
        ];

        return view('cms.blog.index', compact('meta', 'plugins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $meta = [
            'title'			=> 'Add Blog',
			'breadcrumb'	=> [
				'Dashboard' => "/",
				'Blog'	=> "blog",
				'Add Blog'=> null
			]
        ];

        $data = [
            "id" => "",
            "title" => "",
            "subtitle" => "",
            "foto" => "",
            "isi" => "",
            "penulis" => "",
            "kategori" => "",
            "tag" => "",
        ];

        $data = (object) $data;

        $kategori = KategoriBlog::get();

        return view('cms.blog.form', compact('meta', "data", 'kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'foto' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ],
        [
            'foto.image' => 'The type of the uploaded file should be an image.',
            'foto.uploaded' => 'Failed to upload an image. The image maximum size is 2MB.'
        ]);
        
        $blog = New Blog();

        $blog->title = $request->input('title');
        $blog->subtitle = $request->input('subtitle');
        $blog->kategori = $request->input('kategori');
        $blog->isi = $request->input('isi');
        $blog->penulis = getGuardCms()->nama_lengkap;

        if ($request->tag != null) {
            $blog->tag = implode(',', $request->tag);
            
            // tag trigger to table tag
            foreach ($request->tag as $key => $value) {
                $cekTag = Tag::where('tag', $value)->first();
                if ($cekTag == null) {
                    Tag::create(['tag' => $value]);            
                }
            }
        }

        $folder = 'images/blog/';
        if ($request->file('foto')) {
            $blog->foto = uploadImg($request, 'foto', $folder);
        }
        
        $blog->save();
        return redirect()->route('blog.index')->with('success','Data Blog Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Blog::where('id',$id)->first();

        $meta = [
            'title'			=> 'Edit Blog',
			'breadcrumb'	=> [
				'Dashboard'     => "/",
				'Blog'		=> "blog",
				'Edit Blog'	=> ""
			]
        ];

        $kategori = KategoriBlog::get();

        return view('cms.blog.form', compact('meta', "data", 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'foto' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ],
        [
            'foto.image' => 'The type of the uploaded file should be an image.',
            'foto.uploaded' => 'Failed to upload an image. The image maximum size is 2MB.'
        ]);
        
        $blog = [
            'title' => $request->input('title'),
            'subtitle' => $request->input('subtitle'),
            'kategori' => $request->input('kategori'),
            'isi' => $request->input('isi'),
        ];

        $getData = Blog::where('id', $id)->first();
        $nama_old = $getData->foto;
        $folder = 'images/blog/';
        if ($request->file('foto')) {
            $blog['foto'] = uploadImg($request, 'foto', $folder);
            if (file_exists($folder.$nama_old) && $nama_old != null) {
                unlink($folder.$nama_old);
            }
        }

        if ($request->tag != null) {
            $blog['tag'] = implode(',', $request->tag);
            foreach ($request->tag as $key => $value) {
                $cekTag = Tag::where('tag', $value)->first();
                if ($cekTag == null) {
                    Tag::create(['tag' => $value]);            
                }
            }
        }

        Blog::where("id", $id)->update($blog);

        return redirect()->route('blog.index')->with('success','Data Blog Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = Blog::find($id);

        $folder = 'images/blog/';
        $nama_old = $blog->foto;
        if (file_exists($folder.$nama_old) && $nama_old != null) {
            unlink($folder.$nama_old);
        }

        $blog->delete();
        return 'success';
    }

    // tag using select2
    public function tag(Request $request)
    {
        $data = Tag::select('id', 'tag')->limit(5)->get();

        if($request->has('q')){
            $term = $request->q;
            $data = Tag::select('id', 'tag')->where('tag','LIKE','%'.$term.'%')->limit(5)->get();
        }

        return response()->json($data);
    }
}
