<?php

namespace App\Http\Controllers\cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class AuthController extends Controller
{
    public function login()
    {
        if (getGuardCms() != null)
            return redirect('/');

        return view('cms.auth.login');
    }

    public function postLogin(Request $request)
    {
        $user = User::where('username', $request->username)->first();

        if ($user != null) {
            $check_login = auth()->guard($user->role)->attempt(['username' =>  $request->input('username'), 'password' => $request->input('password')]);
            if ($check_login) {
                if (auth()->guard($user->role)->user()->status != 'Aktif') {
                    auth()->guard($user->role)->logout();
                    return redirect()->route('loginForm')->with('errorss', ['Akun Tidak Aktif.']);
                } else {
                    $user = User::where('id', auth()->guard($user->role)->user()->id)->first();
                    return redirect('/')->with('success', 'Berhasil Login !! ');
                }
            } else {
                return redirect()->route('loginForm')->with('errorss', ['password salah.']);
            }
        } else {
            return redirect()->route('loginForm')->with('errorss', ['Akun tidak terdaftar.']);
        }
    }

    public function logout()
    {
        auth()->guard(getGuardCms()->role)->logout();
        return redirect()->route('loginForm');
    }
}
