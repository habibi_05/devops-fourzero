<?php

namespace App\Http\Controllers\cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UsersMembers;
use DataTables;
use Illuminate\Support\Facades\Hash;

class MemberController extends Controller
{

    /**
     * Display a listing of the data with Json.
     *
     * @return \Illuminate\Http\Response
     */
    function dataJson(){
        return Datatables::of(UsersMembers::all())
        ->addColumn('action', function ($row) {
            $action = '<a style="margin-right:2px;" href="'.url('member/'.$row->id.'/edit').'" class="btn btn-inverse-primary"><i class="bx bx-edit me-0"></i></a>';
            $action .= '<button data-remote="'.url('member/'. $row->id).'" class="btn btn-delete btn-inverse-danger"><i class="bx bx-trash me-0"></i></button>';
            
            return $action;
        })
        ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta = [
            'title'			=> 'Member',
			'breadcrumb'	=> [
				'Dashboard' => "/",
				'Member'	=> null
			]
        ];

        $plugins = [
            'css' => [
                'dashboard/plugins/datatable/css/dataTables.bootstrap5.min.css',
                'dashboard/plugins/notifications/css/lobibox.min.css'
            ],
            'js' => [
                'dashboard/plugins/datatable/js/jquery.dataTables.min.js',
                'dashboard/plugins/datatable/js/dataTables.bootstrap5.min.js',
                'dashboard/plugins/notifications/js/lobibox.min.js',
                'dashboard/plugins/sweetalert/sweetalert.min.js',
                'dashboard/plugins/sweetalert/jquery.sweet-alert.custom.js',
            ]
        ];

        return view('cms.member.index', compact('meta', 'plugins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = UsersMembers::where('id',$id)->first();

        $meta = [
            'title'			=> 'Edit Member',
			'breadcrumb'	=> [
				'Dashboard'     => "/",
				'Member'		=> "Member",
				'Edit Member'	=> $data->first_name." ".$data->last_name
			]
        ];

        return view('cms.member.form', compact('meta', "data"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required',
            'status' =>'required',
        ]);

        $isUniqueField = UsersMembers::where([
            ['email', $request->input('email')],
            ['id', "!=",  $id]
        ])->count();

        if ($isUniqueField) {
            return redirect()->back()->with('errorss', ['Email sudah digunakan']);
        }
        
        $user = [
            'first_name' => $request->input('firstname'),
            'last_name' => $request->input('lastname'),
            'email' => $request->input('email'),
            'status' => $request->input('status'),
        ];
        
        if ($request->input('password')) {
            $request->validate([
                'password' => 'required|min:4',
            ]);
            $user['password'] =  Hash::make($request->input('password'));
        };

        UsersMembers::where("id", $id)->update($user);

        return redirect()->route('member.index')->with('success','Data Member Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = UsersMembers::where('id',$id);
        $user->delete();
        return 'success';
    }
}
