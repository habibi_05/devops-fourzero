<?php

namespace App\Http\Controllers\cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SiteManagement;
use Image;

class HomePageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta = [
            'title'			=> 'Homepage',
			'breadcrumb'	=> [
				'Dashboard' => "/",
				'Homepage'	=> null
			]
        ];

        $plugins = [
            'js' => [
                'dashboard/plugins/sweetalert/sweetalert.min.js',
                'dashboard/plugins/sweetalert/jquery.sweet-alert.custom.js',
            ]
        ];

        $header = SiteManagement::where('keyname', 'header')->first();
        $section1 = SiteManagement::where('keyname', 'section1')->first();
        $section2 = SiteManagement::where('keyname', 'section2')->first();
        $section3 = SiteManagement::where('keyname', 'section3')->first();
        $section4 = SiteManagement::where('keyname', 'section4')->first();

        $header['icon'] = json_decode($header->icon);
        $header['value'] = json_decode($header->value);

        $section1['value'] = json_decode($section1->value);

        $section2['icon'] = json_decode($section2->icon);
        $section2['value'] = json_decode($section2->value);

        $section3['value'] = json_decode($section3->value);

        $section4['icon'] = json_decode($section4->icon);
        $section4['value'] = json_decode($section4->value);

        return view('cms.homepage.index', compact('meta', 'plugins', 'header', 'section1', 'section2', 'section3', 'section4'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $keyname = "";
        $data = [];
        foreach ($request->all() as $key => $value) {
            if ($key != "_token") {
                // generate keyname name
                $explodeKey = explode("_", $key);
                if ($explodeKey[0] != $keyname) {
                    $keyname = $explodeKey[0];

                    // status section = 0 or 1
                    $status = $value == 'on' ? '1' : '0';
                    $status = $keyname == 'header' ? '1' : $status;
                    if (array_key_exists("status", $data)) {
                        $data["status"] += [$keyname => $status];
                    } else {
                        $data += [
                            "status" => [$keyname => $status]
                        ];
                    }
                }

                $getData = SiteManagement::where('keyname', $keyname)->first();

                if ($explodeKey[1] == "icon") {
                    // upload multiple foto
                    $folder = 'images/home/';
                    if ($request->file($key)) {
                        $request->validate([
                            $key.'.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                        ],
                        [
                            $key.'.image' => 'The type of the uploaded file should be an image.',
                            $key.'.uploaded' => 'Failed to upload an image. The image maximum size is 2MB.'
                        ]);

                        $foto = uploadImgMultiple($request, $key, $folder);
                        
                        if (count($foto) > 0) {
                            $icons = [];
                            if ($getData->icon != null) {
                                foreach (json_decode($getData->icon) as $key => $value) {
                                    if (array_key_exists($key, $foto)) {
                                        array_push($icons, $foto[$key]);
                                        unlink($folder.$value);
                                    } else {
                                        array_push($icons, $value);
                                    }
                                }
                            } else {
                                array_push($icons, $foto);
                            }
                            
                            
                            if (array_key_exists("icon", $data)) {
                                $data["icon"] += [$keyname => $icons];
                            } else {
                                $data += [
                                    "icon" => [$keyname => $icons]
                                ];
                            }
                        }
                    } else {
                        if ($getData) {
                            $data += [
                                "icon" => [$keyname => json_decode($getData->icon)]
                            ];
                        }
                    }
                } elseif ($explodeKey[1] == "image") {
                    // upload single foto
                    $folder = 'images/home/';
                    if ($request->file($key)) {
                        $foto = uploadImg($request, $key, $folder);
                        
                        if ($getData->image != null && file_exists($folder.$getData->image)) {
                            unlink($folder.$getData->image);
                        }
                        
                        if (array_key_exists("image", $data)) {
                            $data["image"] += [$keyname => $foto];
                        } else {
                            $data += [
                                "image" => [$keyname => $foto]
                            ];
                        }
                    } else {
                        if ($getData) {
                            $data += [
                                "image" => [$keyname => $getData->image]
                            ];
                        }
                    }
                } else {
                    // set data
                    if (array_key_exists("value", $data)) {
                        if (array_key_exists($keyname, $data['value'])) {
                            $data["value"][$keyname] += [
                                $explodeKey[1] => $value
                            ];
                        } else {
                            $data["value"] += [
                                $keyname => [
                                    $explodeKey[1] => $value
                                ]
                            ];
                        }
                        
                    } else {
                        $data += [
                            "value" => [
                                $keyname => [
                                    $explodeKey[1] => $value
                                ]
                            ]
                        ];
                    }
                }

            }
        }

        foreach ($data['value'] as $keyname => $value) {
            $dataPost = [];
            $dataPost['value'] = $value ? json_encode($value) : null;

            if (array_key_exists('image', $data)) {
                if (array_key_exists($keyname, $data['image'])) {
                    $dataPost['image'] = $data['image'][$keyname];
                }
            }

            if (array_key_exists('icon', $data)) {
                if (array_key_exists($keyname, $data['icon'])) {
                    $dataPost['icon'] = json_encode($data['icon'][$keyname]);
                }
            }

            if (array_key_exists('status', $data)) {
                if (array_key_exists($keyname, $data['status'])) {
                    $dataPost['status'] = $data['status'][$keyname];
                }
            }
            
            SiteManagement::updateOrCreate(
                ['section' => 'homepage', 'keyname' => $keyname],
                $dataPost
            );
        }

        return redirect()->route('homepage.index')->with('success','Data Homepage Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
