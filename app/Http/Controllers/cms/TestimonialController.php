<?php

namespace App\Http\Controllers\cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Testimonial;
use DataTables;

class TestimonialController extends Controller
{

    /**
     * Display a listing of the data with Json.
     *
     * @return \Illuminate\Http\Response
     */
    function dataJson(){
        return Datatables::of(Testimonial::all())
        ->addColumn('action', function ($row) {
            $action = '<a style="margin-right:2px;" href="'.url('testimonial/'.$row->id.'/edit').'" class="btn btn-inverse-primary"><i class="bx bx-edit me-0"></i></a>';
            $action .= '<button data-remote="'.url('testimonial/'. $row->id).'" class="btn btn-delete btn-inverse-danger"><i class="bx bx-trash me-0"></i></button>';
            
            return $action;
        })
        ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta = [
            'title'			=> 'Testimonial',
			'breadcrumb'	=> [
				'Dashboard' => "/",
				'Testimonial'	=> null
			]
        ];

        $plugins = [
            'css' => [
                'dashboard/plugins/datatable/css/dataTables.bootstrap5.min.css',
            ],
            'js' => [
                'dashboard/plugins/datatable/js/jquery.dataTables.min.js',
                'dashboard/plugins/datatable/js/dataTables.bootstrap5.min.js',
                'dashboard/plugins/sweetalert/sweetalert.min.js',
                'dashboard/plugins/sweetalert/jquery.sweet-alert.custom.js',
            ]
        ];

        return view('cms.testimonial.index', compact('meta', 'plugins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $meta = [
            'title'			=> 'Add Testimonial',
			'breadcrumb'	=> [
				'Dashboard'         => "/",
				'Testimonial'		=> "testimonial",
				'Add Testimonial'	=> null
			]
        ];

        $data = [
            "id" => "",
            "name" => "",
            "position" => "",
            "description" => "",
            "foto" => "",
        ];

        $data = (object) $data;

        return view('cms.testimonial.form', compact('meta', "data"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'foto' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ],
        [
            'foto.image' => 'The type of the uploaded file should be an image.',
            'foto.uploaded' => 'Failed to upload an image. The image maximum size is 2MB.'
        ]);
        
        $testimonial = New Testimonial();

        $testimonial->name = $request->input('name');
        $testimonial->position = $request->input('position');
        $testimonial->description = $request->input('description');

        $folder = 'images/testimonial/';
        if ($request->file('foto')) {
            $testimonial->foto = uploadImg($request, 'foto', $folder);
        }
        
        $testimonial->save();
        return redirect()->route('testimonial.index')->with('success','Data Testimonial Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Testimonial::where('id',$id)->first();

        $meta = [
            'title'			=> 'Edit Testimonial',
			'breadcrumb'	=> [
				'Dashboard'     => "/",
				'Testimonial'		=> "testimonial",
				'Edit Testimonial'	=> ""
			]
        ];

        return view('cms.testimonial.form', compact('meta', "data"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'foto' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ],
        [
            'foto.image' => 'The type of the uploaded file should be an image.',
            'foto.uploaded' => 'Failed to upload an image. The image maximum size is 2MB.'
        ]);
        
        $testimonial = [
            'name' => $request->input('name'),
            'position' => $request->input('position'),
            'description' => $request->input('description'),
        ];

        $getData = Testimonial::where('id', $id)->first();
        $nama_old = $getData->foto;
        $folder = 'images/testimonial/';
        if ($request->file('foto')) {
            $testimonial['foto'] = uploadImg($request, 'foto', $folder);
            if (file_exists($folder.$nama_old) && $nama_old != null) {
                unlink($folder.$nama_old);
            }
        }

        Testimonial::where("id", $id)->update($testimonial);

        return redirect()->route('testimonial.index')->with('success','Data Testimonial Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $testimonial = Testimonial::find($id);

        $folder = 'images/testimonial/';
        $nama_old = $testimonial->foto;
        if (file_exists($folder.$nama_old) && $nama_old != null) {
            unlink($folder.$nama_old);
        }

        $testimonial->delete();
        return 'success';
    }
}
