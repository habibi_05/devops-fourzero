<?php

namespace App\Http\Controllers\cms;

use App\Http\Controllers\Controller;
use App\Mail\ContactEmail;
use Illuminate\Http\Request;
use DataTables;
use App\Models\Contact;

class ContactEmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function dataJson(){
        return Datatables::of(Contact::orderByDesc('id')->get())
        ->addColumn('action', function ($row) {
            $action = '<a style="margin-right:2px;" href="'.url('contact-email/'.$row->id).'" class="btn btn-inverse-primary"><i class="bx bx-show me-0"></i></a>';
            $action .= '<button data-remote="'.url('contact-email/'. $row->id).'" class="btn btn-delete btn-inverse-danger"><i class="bx bx-trash me-0"></i></button>';
            return $action;
        })
        ->addColumn('indo_date', function ($row) {
            return tanggalIndo($row->created_at);
        })
        ->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta = [
            'title'			=> 'Contact Email',
			'breadcrumb'	=> [
				'Dashboard' => "/",
				'Contact Email'	=> null
			]
        ];

        $plugins = [
            'css' => [
                'dashboard/plugins/datatable/css/dataTables.bootstrap5.min.css'
            ],
            'js' => [
                'dashboard/plugins/datatable/js/jquery.dataTables.min.js',
                'dashboard/plugins/datatable/js/dataTables.bootstrap5.min.js',
                'dashboard/plugins/sweetalert/sweetalert.min.js',
                'dashboard/plugins/sweetalert/jquery.sweet-alert.custom.js',
            ]
        ];

        return view('cms.contact-email.index', compact('meta', 'plugins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Contact::where('id',$id)->first();

        $meta = [
            'title'			=> 'Detail Contact Email',
			'breadcrumb'	=> [
				'Dashboard' => "/",
				'Contact Email'	=> "contact-email",
				'Detail Contact Email'	=> null
			]
        ];

        return view('cms.contact-email.show', compact('meta', "data"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = Contact::where('id',$id);
        $contact->delete();
        return 'success';
    }
}
