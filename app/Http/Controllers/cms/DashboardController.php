<?php

namespace App\Http\Controllers\cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $meta = [
            'title' => 'Dashboard'
        ];

        $plugins = [
            'css' => [
                'dashboard/plugins/vectormap/jquery-jvectormap-2.0.2.css'
            ],
            'js' => [
                'dashboard/plugins/vectormap/jquery-jvectormap-2.0.2.min.js',
                'dashboard/plugins/vectormap/jquery-jvectormap-world-mill-en.js',
                'dashboard/plugins/chartjs/js/Chart.min.js',
                'dashboard/plugins/chartjs/js/Chart.extension.js',
                'dashboard/js/index.js',
            ]
        ];

        return view('cms/dashboard.index', compact('meta', 'plugins'));
    }
}
