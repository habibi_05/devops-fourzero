<?php

namespace App\Http\Controllers\cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Event;
use DataTables;
use Image;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function dataJson(){
        return Datatables::of(Event::all())
        ->addColumn('action', function ($row) {
            $action = '<a style="margin-right:2px;" href="'.url('event/'.$row->id.'/edit').'" class="btn btn-inverse-primary"><i class="bx bx-edit me-0"></i></a>';
            $action .= '<button data-remote="'.url('event/'. $row->id).'" class="btn btn-delete btn-inverse-danger"><i class="bx bx-trash me-0"></i></button>';
            
            return $action;
        })
        ->make(true);
    }

    public function index()
    {
        $meta = [
            'title'			=> 'Event',
			'breadcrumb'	=> [
				'Dashboard' => "/",
				'Event'	=> null
			]
        ];

        $plugins = [
            'css' => [
                'dashboard/plugins/datatable/css/dataTables.bootstrap5.min.css',
                'dashboard/plugins/notifications/css/lobibox.min.css'
            ],
            'js' => [
                'dashboard/plugins/datatable/js/jquery.dataTables.min.js',
                'dashboard/plugins/datatable/js/dataTables.bootstrap5.min.js',
                'dashboard/plugins/notifications/js/lobibox.min.js',
                'dashboard/plugins/sweetalert/sweetalert.min.js',
                'dashboard/plugins/sweetalert/jquery.sweet-alert.custom.js',
            ]
        ];

        return view('cms.event.index', compact('meta', 'plugins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $meta = [
            'title'			=> 'Add Event',
			'breadcrumb'	=> [
				'Dashboard' => "/",
				'Event'	=> "event",
				'Add Event'=> null
			]
        ];

        $data = [
            "id" => "",
            "title" => "",
            "foto" => "",
            "deskripsi" => "",
            "tanggal" => "",
            "harga" => "",
            "duration" => "",
            "link" => "",
        ];

        $data = (object) $data;

        return view('cms.event.form', compact('meta', "data"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'foto' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ],
        [
            'foto.image' => 'The type of the uploaded file should be an image.',
            'foto.uploaded' => 'Failed to upload an image. The image maximum size is 2MB.',
        ]);

        $event = New Event();

        $event->title = $request->input('title');
        $event->tanggal = $request->input('tanggal');
        $event->harga = $request->input('harga');
        $event->duration = $request->input('duration');
        $event->link = $request->input('link');
        $event->deskripsi = $request->input('deskripsi');

        $folder = 'images/event/';
        if ($request->file('foto')) {
            $event->foto = uploadImg($request, 'foto', $folder);
        }
        
        $event->save();
        return redirect()->route('event.index')->with('success','Data Event Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Event::where('id',$id)->first();

        $meta = [
            'title'			=> 'Edit Event',
			'breadcrumb'	=> [
				'Dashboard'     => "/",
				'Event'		=> "event",
				'Edit Event'	=> ""
			]
        ];

        return view('cms.event.form', compact('meta', "data"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'foto' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ],
        [
            'foto.image' => 'The type of the uploaded file should be an image.',
            'foto.uploaded' => 'Failed to upload an image. The image maximum size is 2MB.',
        ]);

        $event = [
            'title' => $request->input('title'),
            'tanggal' => $request->input('tanggal'),
            'harga' => $request->input('harga'),
            'duration' => $request->input('duration'),
            'link' => $request->input('link'),
            'deskripsi' => $request->input('deskripsi'),
        ];

        $getData = Event::where('id', $id)->first();
        $nama_old = $getData->foto;
        $folder = 'images/event/';
        if ($request->file('foto')) {
            $event['foto'] = uploadImg($request, 'foto', $folder);
            if (file_exists($folder.$nama_old) && $nama_old != null) {
                unlink($folder.$nama_old);
            }
        }
        
        Event::where("id", $id)->update($event);
        return redirect()->route('event.index')->with('success','Data Event Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::find($id);

        $folder = 'images/event/';
        $nama_old = $event->foto;
        if (file_exists($folder.$nama_old) && $nama_old != null) {
            unlink($folder.$nama_old);
        }

        $event->delete();
        return 'success';
    }
}
