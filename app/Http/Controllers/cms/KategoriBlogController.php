<?php

namespace App\Http\Controllers\cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\KategoriBlog;
use DataTables;

class KategoriBlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function dataJson(){
        return Datatables::of(KategoriBlog::all())
        ->addColumn('action', function ($row) {
            $action = '<a style="margin-right:2px;" href="'.url('kategori_blog/'.$row->id.'/edit').'" class="btn btn-inverse-primary"><i class="bx bx-edit me-0"></i></a>';
            $action .= '<button data-remote="'.url('kategori_blog/'. $row->id).'" class="btn btn-delete btn-inverse-danger"><i class="bx bx-trash me-0"></i></button>';
            
            return $action;
        })
        ->make(true);
    }

    public function index()
    {
        $meta = [
            'title'			=> 'Kategori Blog',
			'breadcrumb'	=> [
				'Dashboard' => "/",
				'Kategori Blog'	=> null
			]
        ];

        $plugins = [
            'css' => [
                'dashboard/plugins/datatable/css/dataTables.bootstrap5.min.css',
                'dashboard/plugins/notifications/css/lobibox.min.css'
            ],
            'js' => [
                'dashboard/plugins/datatable/js/jquery.dataTables.min.js',
                'dashboard/plugins/datatable/js/dataTables.bootstrap5.min.js',
                'dashboard/plugins/notifications/js/lobibox.min.js',
                'dashboard/plugins/sweetalert/sweetalert.min.js',
                'dashboard/plugins/sweetalert/jquery.sweet-alert.custom.js',
            ]
        ];

        return view('cms.kategori-blog.index', compact('meta', 'plugins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $meta = [
            'title'			=> 'Add Kategori Blog',
			'breadcrumb'	=> [
				'Dashboard' => "/",
				'Kategori Blog'	=> "kategori_blog",
				'Add Kategori Blog'=> null
			]
        ];

        $data = [
            "id" => "",
            "kategori" => "",
        ];

        $data = (object) $data;

        return view('cms.kategori-blog.form', compact('meta', "data"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kategori_blog = New KategoriBlog();

        $kategori_blog->kategori = $request->input('kategori');
        
        $kategori_blog->save();
        return redirect()->route('kategori_blog.index')->with('success','Data Kategori Blog Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = KategoriBlog::where('id',$id)->first();

        $meta = [
            'title'			=> 'Edit Kategori_blog',
			'breadcrumb'	=> [
				'Dashboard'     => "/",
				'Kategori Blog'		=> "Kategori_blog",
				'Edit Kategori Blog'	=> $data->title
			]
        ];

        return view('cms.kategori-blog.form', compact('meta', "data"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $kategori_blog = [
            'kategori' => $request->input('kategori'),
        ];

        KategoriBlog::where("id", $id)->update($kategori_blog);

        return redirect()->route('kategori_blog.index')->with('success','Data Kategori Blog Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategori_blog = KategoriBlog::where('id',$id);
        $kategori_blog->delete();
        return 'success';
    }
}
