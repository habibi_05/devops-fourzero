<?php

namespace App\Http\Controllers\cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Library;
use DataTables;

class LibraryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function dataJson(){
        return Datatables::of(Library::all())
        ->addColumn('action', function ($row) {
            $action = '<a style="margin-right:2px;" href="'.url('library/'.$row->id.'/edit').'" class="btn btn-inverse-primary"><i class="bx bx-edit me-0"></i></a>';
            $action .= '<button data-remote="'.url('library/'. $row->id).'" class="btn btn-delete btn-inverse-danger"><i class="bx bx-trash me-0"></i></button>';
            
            return $action;
        })
        ->make(true);
    }

    public function index()
    {
        $meta = [
            'title'			=> 'Library',
			'breadcrumb'	=> [
				'Dashboard' => "/",
				'Library'	=> null
			]
        ];

        $plugins = [
            'css' => [
                'dashboard/plugins/datatable/css/dataTables.bootstrap5.min.css',
                'dashboard/plugins/notifications/css/lobibox.min.css'
            ],
            'js' => [
                'dashboard/plugins/datatable/js/jquery.dataTables.min.js',
                'dashboard/plugins/datatable/js/dataTables.bootstrap5.min.js',
                'dashboard/plugins/notifications/js/lobibox.min.js',
                'dashboard/plugins/sweetalert/sweetalert.min.js',
                'dashboard/plugins/sweetalert/jquery.sweet-alert.custom.js',
            ]
        ];

        return view('cms.library.index', compact('meta', 'plugins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $meta = [
            'title'			=> 'Add Library',
			'breadcrumb'	=> [
				'Dashboard' => "/",
				'Library'	=> "library",
				'Add Library'=> null
			]
        ];

        $data = [
            "id" => "",
            "nama" => "",
            "deskripsi" => "",
            "foto" => "",
            "file" => "",
        ];

        $data = (object) $data;

        return view('cms.library.form', compact('meta', "data"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'foto' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'file' => 'mimes:pdf'
        ],
        [
            'foto.image' => 'The type of the uploaded file should be an image.',
            'foto.uploaded' => 'Failed to upload an image. The image maximum size is 2MB.',
            'file' => 'The type of the uploaded file should be an pdf.'
        ]);

        $library = New Library();

        $library->nama = $request->input('nama');
        $library->deskripsi = $request->input('deskripsi');

        $folder = 'images/library/';
        if ($request->file('foto')) {
            $library->foto = uploadImg($request, 'foto', $folder);
        }

        $folder2 = 'pdf/';
        if ($request->file('file')) {
            $library->file = uploadPdf($request, 'file', $folder2);
        }
        
        $library->save();
        return redirect()->route('library.index')->with('success','Data Library Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Library::where('id',$id)->first();

        $meta = [
            'title'			=> 'Edit Library',
			'breadcrumb'	=> [
				'Dashboard'     => "/",
				'Library'		=> "library",
				'Edit Library'	=> ""
			]
        ];

        return view('cms.library.form', compact('meta', "data"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'foto' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'file' => 'mimes:pdf'
        ],
        [
            'foto.image' => 'The type of the uploaded file should be an image.',
            'foto.uploaded' => 'Failed to upload an image. The image maximum size is 2MB.',
            'file' => 'The type of the uploaded file should be an pdf.'
        ]);

        $library = [
            'nama' => $request->input('nama'),
            'deskripsi' => $request->input('deskripsi'),
        ];

        $getData = Library::where('id', $id)->first();
        $nama_old = $getData->foto;
        $folder = 'images/library/';
        if ($request->file('foto')) {
            $library['foto'] = uploadImg($request, 'foto', $folder);
            if (file_exists($folder.$nama_old) && $nama_old != null) {
                unlink($folder.$nama_old);
            }
        }

        $nama_old2 = $getData->file;
        $folder2 = 'pdf/';
        if ($request->file('file')) {
            $library['file'] = uploadPdf($request, 'file', $folder2);
            if (file_exists($folder2.$nama_old2) && $nama_old2 != null) {
                unlink($folder2.$nama_old2);
            }
        }
        
        Library::where("id", $id)->update($library);
        return redirect()->route('library.index')->with('success','Data Library Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $library = Library::find($id);

        $folder = 'images/library/';
        $nama_old = $library->foto;
        if (file_exists($folder.$nama_old) && $nama_old != null) {
            unlink($folder.$nama_old);
        }

        $folder2 = 'pdf/';
        $nama_old2 = $library->file;
        if (file_exists($folder2.$nama_old2) && $nama_old2 != null) {
            unlink($folder2.$nama_old2);
        }

        $library->delete();
        return 'success';
    }
}
