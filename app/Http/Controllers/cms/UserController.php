<?php

namespace App\Http\Controllers\cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use DataTables;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{

    /**
     * Display a listing of the data with Json.
     *
     * @return \Illuminate\Http\Response
     */
    function dataJson(){
        return Datatables::of(User::all())
        ->addColumn('action', function ($row) {
            $action = '<a style="margin-right:2px;" href="'.url('user/'.$row->id.'/edit').'" class="btn btn-inverse-primary"><i class="bx bx-edit me-0"></i></a>';
            $action .= '<button data-remote="'.url('user/'. $row->id).'" class="btn btn-delete btn-inverse-danger"><i class="bx bx-trash me-0"></i></button>';
            
            return $action;
        })
        ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta = [
            'title'			=> 'User',
			'breadcrumb'	=> [
				'Dashboard' => "/",
				'User'		=> null
			]
        ];

        $plugins = [
            'css' => [
                'dashboard/plugins/datatable/css/dataTables.bootstrap5.min.css',
                'dashboard/plugins/notifications/css/lobibox.min.css'
            ],
            'js' => [
                'dashboard/plugins/datatable/js/jquery.dataTables.min.js',
                'dashboard/plugins/datatable/js/dataTables.bootstrap5.min.js',
                'dashboard/plugins/notifications/js/lobibox.min.js',
                'dashboard/plugins/sweetalert/sweetalert.min.js',
                'dashboard/plugins/sweetalert/jquery.sweet-alert.custom.js',
            ]
        ];

        return view('cms.user.index', compact('meta', 'plugins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $meta = [
            'title'			=> 'Add User',
			'breadcrumb'	=> [
				'Dashboard' => "/",
				'User'		=> "user",
				'Add User'	=> null
			]
        ];

        $data = [
            "id" => "",
            "username" => "",
            "nama_lengkap" => "",
            "email" => "",
            "status" => "",
            "role" => "",
        ];

        $data = (object) $data;

        return view('cms.user.form', compact('meta', "data"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_lengkap' => 'required',
            'email' => 'required|unique:users',
            'username' => 'required|unique:users|alpha_dash|min:4',
            'password'     =>'required|min:4',
            'status'     =>'required',
            'role'     =>'required',
        ]);
        
        $user = New User();

        $user->nama_lengkap = $request->input('nama_lengkap');
        $user->email = $request->input('email');
        $user->username = $request->input('username');
        $user->password =  Hash::make($request->input('password'));
        $user->status = $request->input('status');
        $user->role = $request->input('role');
        
        $user->save();
        return redirect()->route('user.index')->with('success','Data User Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::where('id',$id)->first();

        $meta = [
            'title'			=> 'Edit User',
			'breadcrumb'	=> [
				'Dashboard' => "/",
				'User'		=> "user",
				'Edit User'	=> $data->fullname
			]
        ];

        return view('cms.user.form', compact('meta', "data"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_lengkap' => 'required',
            'username' => 'required|alpha_dash|min:4',
            'email' => 'required',
            'status'     =>'required',
            'role'     =>'required',
        ]);

        $isUniqueField = User::where([
            ['username', $request->input('username')],
            ['id', "!=",  $id]
        ])
        ->orWhere([
            ['email', $request->input('email')],
            ['id', "!=",  $id]
        ])
        ->count();

        if ($isUniqueField) {
            return redirect()->back()->with('errorss', ['Username atau Email sudah digunakan']);
        }
        
        $user = [
            'nama_lengkap' => $request->input('nama_lengkap'),
            'username' => $request->input('username'),
            'email' => $request->input('email'),
            'status' => $request->input('status'),
            'role' => $request->input('role'),
        ];
        
        if ($request->input('password')) {
            $request->validate([
                'password' => 'required|min:4',
            ]);
            $user['password'] =  Hash::make($request->input('password'));
        };

        User::where("id", $id)->update($user);

        return redirect()->route('user.index')->with('success','Data User Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::where('id',$id);
        $user->delete();
        return 'success';
    }
}
