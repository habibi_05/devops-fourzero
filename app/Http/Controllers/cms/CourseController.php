<?php

namespace App\Http\Controllers\cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Course;
use DataTables;

class CourseController extends Controller
{
    /**
     * Display a listing of the data with Json.
     *
     * @return \Illuminate\Http\Response
     */
    function dataJson(){
        return Datatables::of(Course::all())
        ->addColumn('action', function ($row) {
            $action = '<a style="margin-right:2px;" href="'.url('course/'.$row->id.'/edit').'" class="btn btn-inverse-primary"><i class="bx bx-edit me-0"></i></a>';
            $action .= '<button data-remote="'.url('course/'. $row->id).'" class="btn btn-delete btn-inverse-danger"><i class="bx bx-trash me-0"></i></button>';
            
            return $action;
        })
        ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta = [
            'title'			=> 'Course',
			'breadcrumb'	=> [
				'Dashboard' => "/",
				'Course'	=> null
			]
        ];

        $plugins = [
            'css' => [
                'dashboard/plugins/datatable/css/dataTables.bootstrap5.min.css',
                'dashboard/plugins/notifications/css/lobibox.min.css'
            ],
            'js' => [
                'dashboard/plugins/datatable/js/jquery.dataTables.min.js',
                'dashboard/plugins/datatable/js/dataTables.bootstrap5.min.js',
                'dashboard/plugins/notifications/js/lobibox.min.js',
                'dashboard/plugins/sweetalert/sweetalert.min.js',
                'dashboard/plugins/sweetalert/jquery.sweet-alert.custom.js',
            ]
        ];

        return view('cms.course.index', compact('meta', 'plugins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $meta = [
            'title'			=> 'Add Course',
			'breadcrumb'	=> [
				'Dashboard' => "/",
				'Course'	=> "course",
				'Add Course'=> null
			]
        ];

        $data = [
            "id" => "",
            "title" => "",
            "link" => "",
            "status" => "",
        ];

        $data = (object) $data;

        return view('cms.course.form', compact('meta', "data"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:course',
            'link'     =>'required|min:4',
            'status'     =>'required'
        ]);
        
        $course = New Course();

        $course->title = $request->input('title');
        $course->link = $request->input('link');
        $course->status = $request->input('status');
        
        $course->save();
        return redirect()->route('course.index')->with('success','Data Course Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Course::where('id',$id)->first();

        $meta = [
            'title'			=> 'Edit Course',
			'breadcrumb'	=> [
				'Dashboard'     => "/",
				'Course'		=> "course",
				'Edit Course'	=> $data->title
			]
        ];

        return view('cms.course.form', compact('meta', "data"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'link' => 'required',
            'status' =>'required',
        ]);

        $isUniqueField = Course::where([
            ['title', $request->input('title')],
            ['id', "!=",  $id]
        ])
        ->count();

        if ($isUniqueField) {
            return redirect()->back()->with('errorss', ['Title sudah digunakan']);
        }
        
        $course = [
            'title' => $request->input('title'),
            'link' => $request->input('link'),
            'status' => $request->input('status'),
        ];

        Course::where("id", $id)->update($course);

        return redirect()->route('course.index')->with('success','Data Course Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = Course::where('id',$id);
        $course->delete();
        return 'success';
    }
}
