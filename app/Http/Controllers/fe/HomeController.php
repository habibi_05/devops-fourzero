<?php

namespace App\Http\Controllers\fe;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SiteManagement;
use App\Models\Testimonial;
use App\Models\Event;

class HomeController extends Controller
{
    public function index()
    {
        $meta = [
            'title' => 'Fourzero - Online Course'
        ];

        $header = SiteManagement::where('keyname', 'header')->first();
        $section1 = SiteManagement::where('keyname', 'section1')->first();
        $section2 = SiteManagement::where('keyname', 'section2')->first();
        $section3 = SiteManagement::where('keyname', 'section3')->first();
        $section4 = SiteManagement::where('keyname', 'section4')->first();

        $testimonial = Testimonial::orderBy('id', 'desc')->get();

        $header['icon'] = json_decode($header->icon);
        $header['value'] = json_decode($header->value);

        $section1['value'] = json_decode($section1->value);

        $section2['icon'] = json_decode($section2->icon);
        $section2['value'] = json_decode($section2->value);

        $section3['value'] = json_decode($section3->value);

        $section4['icon'] = json_decode($section4->icon);
        $section4['value'] = json_decode($section4->value);

        $event = Event::where('tanggal', '>=', date('y-m-d'))->orderBy('tanggal')->get();

        return view('fe.page.home', compact('meta', 'header', 'section1', 'section2', 'section3', 'section4', 'testimonial', 'event'));
    }
}
