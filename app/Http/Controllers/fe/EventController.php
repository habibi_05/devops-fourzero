<?php

namespace App\Http\Controllers\fe;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Event;

class EventController extends Controller
{
    public function index()
    {
        $meta = [
            'title' => 'Event - Fourzero',
            'pageTitle' => 'Event'
        ];

        $dataEvent = [
            'data' => Event::orderByDesc('id')->limit(5)->get(),
            'dataCount' => count(Event::get())
        ];

        return view('fe.page.event', compact('meta', 'dataEvent'));
    }

    public function detail($id)
    {
        $data = Event::find($id);
        $event = Event::orderByDesc('id')->limit(5)->get();

        $meta = [
            'title' => $data->title.' - Fourzero',
            'pageTitle' => 'Detail Blog'
        ];
        
        return view('fe.page.detail-event', compact('meta', 'data', 'event'));
    }

    public function loadmore(Request $request)
    {
        $limit      = $request->limit;
        $offset     = $request->offset;

        $listData   = Event::orderByDesc('id')->take($limit)->skip($offset)->get();
        $list_loadmore = '';
        if (!empty($listData)) {
            foreach ($listData as $key => $item) {
                $url    = url('event/detail/'.$item->id);
                $list_loadmore .= '
                    <div class="single-event-inner media">
                        <div class="media-left">
                            <img width="213" src="/images/event/'. $item->foto .'" alt="img">
                        </div>
                        <div class="media-body align-self-center">
                            <a class="right-arrow" href="'. $url .'"><i class="fa fa-mail-forward"></i></a>
                            <p class="date">'. $item->duration .'</p>
                            <h4><a href="'. $url .'">'. ucfirst(strLimit($item->title, 75)) .'</a></h4>
                            <p class="content">'. ucfirst(htmlspecialchars(strLimit($item->deskripsi, 200))) .'</p>
                        </div>
                    </div>
                ';
            }
        }
        return response()->json($list_loadmore);
    }

}
