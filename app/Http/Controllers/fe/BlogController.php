<?php

namespace App\Http\Controllers\fe;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;

class BlogController extends Controller
{
    public function index()
    {
        $meta = [
            'title' => 'Blog - Fourzero',
            'pageTitle' => 'Blog'
        ];

        $dataBlog = [
            'data' => Blog::orderByDesc('id')->limit(3)->get(),
            'dataCount' => count(Blog::get())
        ];

        return view('fe.page.blog', compact('meta', 'dataBlog'));
    }

    public function detail($id)
    {
        $data = Blog::find($id);
        $blog = Blog::orderByDesc('id')->limit(3)->get();

        $meta = [
            'title' => $data->title.' - Fourzero',
            'pageTitle' => 'Detail Blog'
        ];
        
        return view('fe.page.detail-blog', compact('meta', 'data', 'blog'));
    }

    public function loadmore(Request $request)
    {
        $limit      = $request->limit;
        $offset     = $request->offset;

        $listData   = Blog::orderByDesc('id')->take($limit)->skip($offset)->get();
        $list_loadmore = '';
        if (!empty($listData)) {
            foreach ($listData as $key => $item) {
                $url    = url('blog/detail/'.$item->id);
                $list_loadmore .= '
                    <div class="single-blog-inner">
                        <div class="thumb">
                            <img src="images/blog/'. $item->foto .'" alt="img">
                        </div>
                        <div class="details">
                            <div class="blog-meta">
                                <ul>
                                    <li><i class="fa fa-user"></i> '. ucwords($item->penulis) .'</li>
                                    <li><i class="fa fa-clock-o"></i> '. tanggalIndo($item->created_at) .'</li>
                                    <li><i class="fa fa-folder-open"></i> '. ucwords($item->kategori) .'</li>
                                </ul>
                            </div>
                            <h4><a href="'. $url .'">'. ucfirst(strLimit($item->subtitle, 25)) .'</a></h4>
                            <p>'. strLimit($item->subtitle, 70) .'</p>
                            <a class="btn btn-base-m" href="'. $url .'">Read More +</a>
                        </div>
                    </div>
                ';
            }
        }
        return response()->json($list_loadmore);
    }

}
