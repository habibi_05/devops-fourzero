<?php

namespace App\Http\Controllers\fe;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UsersMembers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;

use App\Mail\VerifikasiEmail;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    // signup
    public function signup()
    {
        if (getGuardFe()) {
            return redirect('/');
        }

        $meta = [
            'title' => 'Signup - Fourzero',
            'pageTitle' => 'Signup'
        ];

        return view('fe.page.signup', compact('meta'));
    }

    public function signupProcess(Request $request)
    {
        if (getGuardFe()) {
            return redirect('/');
        }

        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|unique:users_members',
            'password'     =>'required|confirmed|min:4',
        ]);
        
        $usersMembers = New UsersMembers();

        $usersMembers->first_name = $request->input('firstname');
        $usersMembers->last_name = $request->input('lastname');
        $usersMembers->email = $request->input('email');
        $usersMembers->password =  Hash::make($request->input('password'));
        
        $usersMembers->save();

        // generate token link
        $tokenPayload = json_encode([
            'id' => $usersMembers->id,
            'email' => $usersMembers->email,
            'created_at' => $usersMembers->created_at
        ]);

        $token = Crypt::encryptString($tokenPayload);
        
        usersMembers::where("id", $usersMembers->id)->update(['token' => $token]);
        
        $link = url("/verify/".$token);

        Mail::to($usersMembers->email)->send(new VerifikasiEmail($link));
        return redirect()->route('signin')->with('success',['Register Berhasil, Silahkan Cek Email Untuk Verifikasi']);
    }


    public function verify($token)
    {
        if (getGuardFe()) {
            return redirect('/');
        }

        try {
            $tokenDecrypt = json_decode(Crypt::decryptString($token));
    
            $user = usersMembers::where(["email" => $tokenDecrypt->email])->first();
    
            if(!$user){
                $meta = [
                    'title' => 'Verify - Fourzero'
                ];

                $message = "Verifikasi gagal, akun tidak ditemukan";
                return view('fe.page.verify', compact('meta', 'message'));
            }
            
            usersMembers::where("id", $tokenDecrypt->id)->update(['status' => "Aktif"]);
    
            return redirect()->route('signin')->with('success',['Verifikasi berhasil, silahkan login']);

        } catch (DecryptException $e) {
            $meta = [
                'title' => 'Verify - Fourzero'
            ];

            $message = "Verifikasi gagal, pastikan link benar";
            return view('fe.page.verify', compact('meta', 'message'));
        }

    }

    // signin
    public function signin()
    {
        if (getGuardFe()) {
            return redirect('/');
        }

        $meta = [
            'title' => 'Signin - Fourzero',
            'pageTitle' => 'Signin'
        ];

        return view('fe.page.signin', compact('meta'));
    }

    public function signinProcess(Request $request)
    {
        if (getGuardFe()) {
            return redirect('/');
        }

        $usersMembers = UsersMembers::where('email', $request->email)->first();
        if($usersMembers!=null){
            $check_login = auth()->guard('member')->attempt(['email' =>  $request->input('email'), 'password' => $request->input('password')]);
            if ($check_login) {
                if(auth()->guard('member')->user()->status != 'Aktif'){
                    auth()->guard('member')->logout();
                    return redirect()->route('signin')->with('errors', ['Akun Belum Aktif, Silahkan Cek Email Untuk Verifikasi']);
                } else {
                    $usersMembers = UsersMembers::where('id', auth()->guard('member')->user()->id)->first();
                    return redirect('/')->with('success', ['Berhasil Login !! Welcome '.$usersMembers->first_name]);
                }
            }else {
                return redirect()->route('signin')->with('errors', ['password salah.']);
            }
        } else {
            return redirect()->route('signin')->with('errors', ['Akun tidak terdaftar.']);
        }
    }

    // signout
    public function signout(){
        auth()->guard('member')->logout();
        return redirect('/');
    }
}
