<?php

namespace App\Http\Controllers\fe;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contact;

use App\Mail\ContactEmail;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function index()
    {
        $meta = [
            'title' => 'Contact - Fourzero',
            'pageTitle' => 'Contact'
        ];

        return view('fe.page.contact', compact('meta'));
    }

    public function send(Request $request)
    {
        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required',
            'message' => 'required'
        ]);
        
        $contact = New Contact();

        $contact->first_name = $request->input('firstname');
        $contact->last_name = $request->input('lastname');
        $contact->email = $request->input('email');
        $contact->message = $request->input('message');
        
        $contact->save();

        $data = [
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
            'email' => $request->input('email'),
            'message' => $request->input('message'),
        ];

        Mail::to($contact->email)->cc(env('MAIL_FROM_ADDRESS'))->send(new ContactEmail($data));
        return redirect(url('contact'))->with('success', ['Contact Berhasil dikirim']);
    }
}
