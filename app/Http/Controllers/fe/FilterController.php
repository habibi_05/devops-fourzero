<?php

namespace App\Http\Controllers\fe;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FilterController extends Controller
{
    public function search(Request $request)
    {
        $keywoard = $request->q;
        
        $meta = [
            'title' => $keywoard.' - Fourzero',
            'pageTitle' => $keywoard
        ];

        $data = $this->getDataSearch($keywoard, 5);

        // dd($data);

        return view('fe.page.search', compact('meta', 'data'));
    }

    public function loadmore(Request $request)
    {
        $keywoard = $request->q;
        $limit    = $request->limit;
        $offset   = $request->offset;

        $listData = $this->getDataSearch($keywoard, $limit, $offset);
        $list_loadmore = '';
        if (!empty($listData)) {
            foreach ($listData as $value) {
                $url    = url($value->type.'/detail/'.$value->id);
                
                if ($value->type == "course" ) {
                    $list_loadmore .= '
                        <div class="single-event-inner media">
                            <div class="media-body align-self-center">
                                <a class="right-arrow" target="_blank" href="'. $value->subtitle .'"><i class="fa fa-mail-forward"></i></a>
                                <h4><a target="_blank" href="'. $value->subtitle .'">'. ucfirst(strLimit($value->title, 75)) .'</a></h4>
                                <h6 class="color-base">'. ucfirst($value->type) .'</h6>
                            </div>
                        </div>
                    ';
                } else {
                    $list_loadmore .= '
                        <div class="single-event-inner media">
                            <div class="media-left">
                                <img width="213" src="'. url('images/'.$value->type.'/'.$value->foto) .'" alt="img">
                            </div>
                            <div class="media-body align-self-center">
                                <a class="right-arrow" href="'. $url .'"><i class="fa fa-mail-forward"></i></a>
                                <h4><a href="'. $url .'">'. ucfirst(strLimit($value->title, 75)) .'</a></h4>
                                <p class="content">'. ucfirst(strLimit($value->subtitle, 200)) .'</p>
                                <p class="date"><a href="'. $url .'" class="font-weight-bold color-base">'. ucfirst($value->type) .'</a> '. tanggalIndo($value->created_at) .'</p>
                            </div>
                        </div>
                    ';
                }
            }
        }
        return response()->json($list_loadmore);
    }

    public function getDataSearch($keywoard, $limit, $offset = 0)
    {
        $blog = DB::table('blog')
                ->select(DB::raw('"blog" as type'), "id", "title", "subtitle", "foto", "kategori", "created_at")
                ->where('title', 'like', "%{$keywoard}%");
        
        $course = DB::table('course')
                ->select(DB::raw('"course" as type'), "id", "title", "link", DB::raw('null as col1'), DB::raw('null as col2'), "created_at")
                ->where('title', 'like', "%{$keywoard}%");
                
        $event = DB::table('event')
                ->select(DB::raw('"event" as type'), "id", "title", "deskripsi as subtitle", "foto", DB::raw('null as col1'), "tanggal as created_at")
                ->where('title', 'like', "%{$keywoard}%");
                
        $library = DB::table('library')
                ->select(DB::raw('"library" as type'), "id", "nama as title", "deskripsi as subtitle", "foto", DB::raw('null as col1'), "created_at")
                ->where('nama', 'like', "%{$keywoard}%")
                ->union($blog)
                ->union($course)
                ->union($event)
                ->orderByDesc('created_at')
                ->take($limit)->skip($offset)
                ->get();
        
        return $library;
    }
}
