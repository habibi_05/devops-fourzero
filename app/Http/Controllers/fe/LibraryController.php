<?php

namespace App\Http\Controllers\fe;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Library;
use App\Models\Course;
use Illuminate\Support\Facades\Storage;
use File;
use Response;

class LibraryController extends Controller
{
    public function index()
    {
        $meta = [
            'title' => 'Library - Fourzero',
            'pageTitle' => 'Library'
        ];

        $dataLibrary = [
            'data' => Library::orderByDesc('id')->limit(6)->get(),
            'dataCount' => count(Library::get())
        ];
        
        return view('fe.page.library', compact('meta', 'dataLibrary'));
    }

    public function detail($id)
    {
        $data = Library::find($id);
        $library = Library::orderByDesc('id')->limit(3)->get();

        $meta = [
            'title' => $data->nama.' - Fourzero',
            'pageTitle' => 'Detail Library'
        ];
        
        return view('fe.page.detail-library', compact('meta', 'data', 'library'));
    }

    public function loadmore(Request $request)
    {
        $limit      = $request->limit;
        $offset     = $request->offset;

        $listData   = Library::orderByDesc('id')->take($limit)->skip($offset)->get();
        $list_loadmore = '';
        if (!empty($listData)) {
            foreach ($listData as $key => $item) {
                $url    = url('library/detail/'.$item->id);
                $list_loadmore .= '
                    <div class="col-lg-4 col-md-6">
                        <div class="single-team-inner text-center">
                            <div class="thumb">
                                <img src="/images/library/'.$item->foto.'" alt="cover">
                            </div>
                            <div class="details">
                                <h4><a href="'.$url.'">'. ucwords($item->nama) .'</a></h4>
                            </div>
                        </div>
                    </div>
                ';
            }
        }
        return response()->json($list_loadmore);
    }

    function download($filename){
        $filepath = public_path('pdf/'.$filename);
        return Response::download($filepath);
    }
}
