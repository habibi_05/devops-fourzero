<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ContentMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!getGuardCms())
            return redirect('login');

        if (getGuardCms()->role == 'admin')
            return $next($request);
        elseif (getGuardCms()->role != 'content')
            return redirect('/');
        else
            return $next($request);
    }
}
