<?php

// cms
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\cms\DashboardController;
use App\Http\Controllers\cms\UserController;
use App\Http\Controllers\cms\AuthController;
use App\Http\Controllers\cms\CourseController;
use App\Http\Controllers\cms\KategoriBlogController;
use App\Http\Controllers\cms\BlogController as CmsBlog;
use App\Http\Controllers\cms\LibraryController as CmsLibrary;
use App\Http\Controllers\cms\EventController as CmsEvent;
use App\Http\Controllers\cms\MemberController;
use App\Http\Controllers\cms\HomePageController;
use App\Http\Controllers\cms\TestimonialController;
use App\Http\Controllers\cms\ContactController;
use App\Http\Controllers\cms\ContactEmailController;

// fe
use App\Http\Controllers\fe\HomeController;
use App\Http\Controllers\fe\BlogController;
use App\Http\Controllers\fe\ContactController as ContactControllerFe;
use App\Http\Controllers\fe\LibraryController;
use App\Http\Controllers\fe\EventController;
use App\Http\Controllers\fe\AuthController as AuthControllerFe;
use App\Http\Controllers\fe\FilterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// CMS
$domainCMS = env('DOMAIN_CMS', 'cms.localhost');
Route::domain($domainCMS)->group(function () {
    // auth
    Route::get('login', [AuthController::class, 'login'])->name('loginForm')->middleware(['guest']);
    Route::post('login/process', [AuthController::class, 'postLogin'])->name('login');
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');

    // Middleware auth login
    Route::middleware(['auth.content'])->group(function() {
        
        Route::get('/', [DashboardController::class, 'index']);
        
        // Middleware auth login role admin
        Route::middleware(['auth.admin'])->group(function() {
            // get tag
            Route::post('blog/selectTag', [CmsBlog::class, 'tag']);

            // get data json
            Route::get('user/dataJson', [UserController::class, 'dataJson']);
            Route::get('member/dataJson', [MemberController::class, 'dataJson']);
            Route::get('course/dataJson', [CourseController::class, 'dataJson']);
            Route::get('blog/dataJson', [CmsBlog::class, 'dataJson']);
            Route::get('library/dataJson', [CmsLibrary::class, 'dataJson']);
            Route::get('event/dataJson', [CmsEvent::class, 'dataJson']);
            Route::get('kategori_blog/dataJson', [KategoriBlogController::class, 'dataJson']);
            Route::get('course/dataJson', [CourseController::class, 'dataJson']);
            Route::get('testimonial/dataJson', [TestimonialController::class, 'dataJson']);
            Route::get('contact-email/dataJson', [ContactEmailController::class, 'dataJson']);

            Route::resource('user', UserController::class);
            Route::resource('member', MemberController::class);
            Route::resource('course', CourseController::class);
            Route::resource('blog', CmsBlog::class);
            Route::resource('library', CmsLibrary::class);
            Route::resource('event', CmsEvent::class);
            Route::resource('kategori_blog', KategoriBlogController::class);
            Route::resource('homepage', HomePageController::class);
            Route::resource('testimonial', TestimonialController::class);
            Route::resource('contact', ContactController::class);
            Route::resource('contact-email', ContactEmailController::class);

        });
    });

    
});

// FE
$domainFE = env('DOMAIN_FE', 'localhost');
Route::domain($domainFE)->group(function () {
    // loadmore
    Route::post('library/loadmore', [LibraryController::class, 'loadmore']);
    Route::post('blog/loadmore', [BlogController::class, 'loadmore']);
    Route::post('event/loadmore', [EventController::class, 'loadmore']);
    Route::post('search/loadmore', [FilterController::class, 'loadmore']);
    
    Route::get('/', [HomeController::class, 'index']);
    Route::get('blog', [BlogController::class, 'index']);
    Route::get('event', [EventController::class, 'index']);
    Route::get('contact', [ContactControllerFe::class, 'index']);
    Route::get('library', [LibraryController::class, 'index']);
    Route::get('search', [FilterController::class, 'search']);

    Route::get('blog/detail/{id}', [BlogController::class, 'detail']);
    Route::get('event/detail/{id}', [EventController::class, 'detail']);
    Route::post('contact/send', [ContactControllerFe::class, 'send']);
    Route::get('library/detail/{id}', [LibraryController::class, 'detail']);
    Route::get('library/download/{filename}', [LibraryController::class, 'download']);

    // auth
    Route::get('verify/{token}', [AuthControllerFe::class, 'verify']);
    
    Route::get('signup', [AuthControllerFe::class, 'signup'])->name('signup');
    Route::post('signup', [AuthControllerFe::class, 'signupProcess'])->name('signupProcess');

    Route::get('signin', [AuthControllerFe::class, 'signin'])->name('signin');
    Route::post('signin', [AuthControllerFe::class, 'signinProcess'])->name('signinProcess');

    Route::get('signout', [AuthControllerFe::class, 'signout'])->name('signout');
});