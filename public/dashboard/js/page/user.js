$(function () {
    $("#table-user").DataTable({
        processing: true,
        serverSide: true,
        ajax: "/user/dataJson",
        columns: [
            // { data: "id", name: "id" },
            { data: "username", name: "username" },
            { data: "nama_lengkap", name: "nama_lengkap" },
            { data: "email", name: "email" },
            {
                data: "status",
                render: function (data, type, row) {
                    if (data == "Aktif") {
                        return '<span class="btn btn-inverse-success">Aktif</span>';
                    } else {
                        return '<span class="btn btn-inverse-danger">Tidak Aktif</span>';
                    }
                },
                className: 'text-center',
            },
            { data: "role", name: "role", className: 'text-center' },
            { data: "action", name: "action", className: 'text-center' },
        ],
    });

    $("#table-user").on("click", ".btn-delete[data-remote]", function (e) {
        e.preventDefault();
        var url = $(this).data("remote");
        console.log(url);
        swal(
            {
                title: "Apakah yakin?",
                text: "Mengapus data user !!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
            },
            function () {
                console.log(url);
                $.ajax({
                    url: url,
                    type: "DELETE",
                    dataType: "json",
                    data: {
                        method: "_DELETE",
                        submit: true,
                        _token: "{{csrf_token()}}",
                    },
                }).always(function (data) {
                    $("#table-user").DataTable().draw(false);
                    swal("Sukses !!", "Menghapus data user.", "success");
                });
            }
        );
    });
});
