$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    // loadmore
	$('#btn-loadmore').click(function (e) {
        if ($(this).data('keywoard') != '') {
            data = {
                'q'         : $(this).data('keywoard'), 
                'limit'     : $(this).data('limit'), 
                'offset'    : $(this).data('offset'),
            }
        } else {
            data = {
                'limit'     : $(this).data('limit'), 
                'offset'    : $(this).data('offset'),
            }
        }
        $.ajax({
            type: "post",
            url: '/'+$(this).data('page') +'/loadmore',
            data: data,
            dataType: "JSON",
            beforeSend:function (response) {
                $('#btn-loadmore').text('Please Wait . . .');
            },
            success: function (response) {
                if (response == "") {
                    $('#btn-loadmore').hide();
                } else {
                    $('#btn-loadmore').data('offset', $('#btn-loadmore').data('offset') + 3);
                    $('#btn-loadmore').text('Loadmore');
                    $('#loadmore').append(response);
                }
            }
        });
    }); 
});