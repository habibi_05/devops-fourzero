@extends('cms.layouts.base')

@section('content')
    <form method="POST" action="{{ route('homepage.store') }}" enctype="multipart/form-data">
        @csrf
        @include('cms.layouts.validation_error')
        {{-- Header --}}
        <div class="card radius-10 border-top border-0 border-3 border-primary">
            <div class="card-body">
                <div class="card-title">
                    <h5 class="mb-0 text-primary">Header</h5>
                    <hr>
                </div>
                <div class="row g-3">
                    <div class="col-md-8">
                        <div class="row mt-2">
                            <div class="col-md-6">
                                <label class="form-label">Title Black</label><strong class="text-danger"> *</strong>
                                <input type="text" name="header_title" class="form-control" value="{{ $header['value']->title }}" placeholder="Welcome To Education" required>
                            </div>
                            <div class="col-md-6">
                                <label class="form-label">Title Purple</label><strong class="text-danger"> *</strong>
                                <input type="text" name="header_subtitle" class="form-control" value="{{ $header['value']->subtitle }}" placeholder="Fourzero.id." required>
                            </div>
                        </div>
                        <div class="mt-2">
                            <label class="form-label">Deskripsi</label>
                            <textarea name="header_description" class="form-control">{{ $header['value']->description }}</textarea>
                        </div>
                        <div class="row mt-2">
                            <div class="col-md-6">
                                <label class="form-label">Button Text</label>
                                <input type="text" name="header_buttontext" class="form-control" placeholder="online course" value="{{ $header['value']->buttontext }}">
                            </div>
                            <div class="col-md-6">
                                <label class="form-label">Button Link</label>
                                <input type="link" name="header_buttonlink" class="form-control" placeholder="https://course.fourzero.id/" value="{{ $header['value']->buttonlink }}">
                            </div>
                        </div>
                        <div class="mt-2">
                            <label class="form-label">Items Icon</label>

                            <div class="row mb-2">
                                @for ($i = 0; $i < 3; $i++)
                                    <div class="col-md-4 text-center">
                                        @if ($header['icon'][$i] != null)
                                            <img src="{{ '/images/home/'.$header['icon'][$i] }}" alt="foto" class="img-fluid w-25 mb-2">
                                        @endif
                                        <input type="text" name="header_icontext[]" class="form-control mb-2" placeholder="Global Classes Facility" value="{{ $header['value']->icontext[$i] }}">
                                        <input type="file" name="header_icon[]" class="form-control form-control-sm" value="">
                                    </div>
                                @endfor
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mt-2">
                            <label class="form-label">Foto</label>
                            @if ($header['image'] != null)
                                <br>
                                <div class="text-center">
                                    <img src="{{ '/images/home/'.$header['image'] }}" alt="foto" class="img-fluid w-50 mb-2">
                                </div>
                            @endif
                            <input type="file" name="header_image" class="form-control" value="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        {{-- Section 1 --}}
        <div class="card radius-10 border-top border-0 border-3 border-primary">
            <div class="card-body">
                <div class="card-title d-flex">
                    <h5 class="mb-0 text-primary">Section 1</h5>
                    <div class="form-check ms-auto">
                        <input class="form-check-input" type="checkbox" name="section1_status" {{ $section1['status'] == '1' ? 'checked' : '' }} id="flexCheckSection1">
                        <label class="form-check-label" for="flexCheckSection1">Aktif</label>
                    </div>
                    <hr>
                </div>
                <div class="row g-3">
                    <div class="col-md-8">
                        <div class="row mt-2">
                            <div class="col-md-6">
                                <label class="form-label">Title Black</label><strong class="text-danger"> *</strong>
                                <input type="text" name="section1_title" class="form-control" placeholder="We're here to help your modern" value="{{ $section1['value']->title }}" required>
                            </div>
                            <div class="col-md-6">
                                <label class="form-label">Title Purple</label><strong class="text-danger"> *</strong>
                                <input type="text" name="section1_subtitle" class="form-control" placeholder="Education" value="{{ $section1['value']->subtitle }}" required>
                            </div>
                        </div>
                        <div class="mt-2">
                            <label class="form-label">Deskripsi</label><strong class="text-danger"> *</strong>
                            <textarea name="section1_description" class="form-control" required>{{ $section1['value']->description }}</textarea>
                        </div>
                        <div class="row mt-2">
                            <div class="col-md-6">
                                <label class="form-label">Button Text</label>
                                <input type="text" name="section1_buttontext" class="form-control" placeholder="Explore More +" value="{{ $section1['value']->buttontext }}">
                            </div>
                            <div class="col-md-6">
                                <label class="form-label">Button Link</label>
                                <input type="link" name="section1_buttonlink" class="form-control" placeholder="https://fourzero.id/" value="{{ $section1['value']->buttonlink }}">
                            </div>
                        </div>
                        <div class="row mt-2">
                            @for ($i = 0; $i < 3; $i++)
                                <div class="col-md-4 {{ $i != 0 ? 'ps-md-0' : '' }}">
                                    <label class="form-label">Items</label>
                                    <input type="text" name="section1_icontext[]" class="form-control mb-2" placeholder="Online Programme" value="{{ $section1['value']->icontext[$i] }}">
                                </div>
                            @endfor
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mt-2">
                            <label class="form-label">Foto</label>
                            @if ($section1['image'] != null)
                                <br>
                                <div class="text-center">
                                    <img src="{{ '/images/home/'.$section1['image'] }}" alt="foto" class="img-fluid w-50 mb-2">
                                </div>
                            @endif
                            <input type="file" name="section1_image" class="form-control" value="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        {{-- Section 2 --}}
        <div class="card radius-10 border-top border-0 border-3 border-primary">
            <div class="card-body">
                <div class="card-title d-flex">
                    <h5 class="mb-0 text-primary">Section 2</h5>
                    <div class="form-check ms-auto">
                        <input class="form-check-input" type="checkbox" name="section2_status" {{ $section2['status'] == '1' ? 'checked' : '' }} id="flexCheckSection2">
                        <label class="form-check-label" for="flexCheckSection2">Aktif</label>
                    </div>
                    <hr>
                </div>
                <div class="row g-3">
                    <div class="col-md-12">
                        <div class="row mt-2">
                            <div class="col-md-6">
                                <label class="form-label">Title Black</label><strong class="text-danger"> *</strong>
                                <input type="text" name="section2_title" class="form-control" value="{{ $section2['value']->title }}" placeholder="Top Courses" required>
                            </div>
                            <div class="col-md-6">
                                <label class="form-label">Title Purple</label><strong class="text-danger"> *</strong>
                                <input type="text" name="section2_subtitle" class="form-control" value="{{ $section2['value']->subtitle }}" placeholder="Categories" required>
                            </div>
                        </div>
    
                        <div class="mt-2">
                            <label class="form-label">Items</label>
                        </div>
                        <div class="row mb-2">
                            @for ($i = 0; $i < 4; $i++)
                                <div class="col-md-3 text-center">
                                    @if ($section2['icon'][$i] != null)
                                        <img src="{{ '/images/home/'.$section2['icon'][$i] }}" alt="foto" class="img-fluid w-25 mb-2">
                                    @endif
                                    <input type="text" name="section2_icontext[]" class="form-control mb-2" value="{{ $section2['value']->icontext[$i] }}" placeholder="Master of Accounting">
                                    <input type="text" name="section2_iconlink[]" class="form-control mb-2" value="{{ $section2['value']->iconlink[$i] }}" placeholder="https://course.fourzero.id/">
                                    <input type="file" name="section2_icon[]" class="form-control mb-2" value="">
                                </div>
                            @endfor
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        {{-- Section 3 --}}
        <div class="card radius-10 border-top border-0 border-3 border-primary">
            <div class="card-body">
                <div class="card-title d-flex">
                    <h5 class="mb-0 text-primary">Section 3</h5>
                    <div class="form-check ms-auto">
                        <input class="form-check-input" type="checkbox" name="section3_status" {{ $section3['status'] == '1' ? 'checked' : '' }} id="flexCheckSection3">
                        <label class="form-check-label" for="flexCheckSection3">Aktif</label>
                    </div>
                    <hr>
                </div>
                <div class="row g-3">
                    <div class="col-md-8">
                        <div class="row mt-2">
                            <div class="col-md-6">
                                <label class="form-label">Title Black</label><strong class="text-danger"> *</strong>
                                <input type="text" name="section3_title" class="form-control" value="{{ $section3['value']->title }}" placeholder="Number & fact how famous Our" required>
                            </div>
                            <div class="col-md-6">
                                <label class="form-label">Title Purple</label><strong class="text-danger"> *</strong>
                                <input type="text" name="section3_subtitle" class="form-control" value="{{ $section3['value']->subtitle }}" placeholder="Programme." required>
                            </div>
                        </div>
                        <div class="mt-2">
                            <label class="form-label">Deskripsi</label><strong class="text-danger"> *</strong>
                            <textarea name="section3_description" class="form-control">{{ $section3['value']->description }}</textarea>
                        </div>
                        <div class="mt-2">
                            <label class="form-label">Items</label>
                            <div class="row mb-2">
                                @for ($i = 0; $i < 3; $i++)
                                    <div class="col-md-4">
                                        <input type="text" name="section3_totalitems[]" class="form-control mb-2" value="{{ $section3['value']->totalitems[$i] }}" placeholder="50">
                                        <input type="text" name="section3_titleitems[]" class="form-control" value="{{ $section3['value']->titleitems[$i] }}" placeholder="courses">
                                    </div>
                                @endfor
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mt-2">
                            <label class="form-label">Foto</label>
                            @if ($section3['image'] != null)
                                <br>
                                <div class="text-center">
                                    <img src="{{ '/images/home/'.$section3['image'] }}" alt="foto" class="img-fluid w-50 mb-2">
                                </div>
                            @endif
                            <input type="file" name="section3_image" class="form-control" value="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        {{-- Section 4 --}}
        <div class="card radius-10 border-top border-0 border-3 border-primary">
            <div class="card-body">
                <div class="card-title d-flex">
                    <h5 class="mb-0 text-primary">Section 4</h5>
                    <div class="form-check ms-auto">
                        <input class="form-check-input" type="checkbox" name="section4_status" {{ $section4['status'] == '1' ? 'checked' : '' }} id="flexCheckSection4">
                        <label class="form-check-label" for="flexCheckSection4">Aktif</label>
                    </div>
                    <hr>
                </div>
                <div class="row g-3">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mt-2">
                                    <label class="form-label">Title Black</label><strong class="text-danger"> *</strong>
                                    <input type="text" name="section4_title" class="form-control" value="{{ $section4['value']->title }}" placeholder="Why Students likes Ours" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mt-2">
                                    <label class="form-label">Title Purple</label><strong class="text-danger"> *</strong>
                                    <input type="text" name="section4_subtitle" class="form-control" value="{{ $section4['value']->subtitle }}" placeholder="Programme." required>
                                </div>
                            </div>
                        </div>
    
                        <div class="row mt-2">
                            <label class="form-label">Items</label>
                            @for ($i = 0; $i < 3; $i++)
                                <div class="col-md-4 text-center">
                                    @if ($section4['icon'][$i] != null)
                                        <img src="{{ '/images/home/'.$section4['icon'][$i] }}" alt="foto" class="img-fluid w-25 mb-2">
                                    @endif
                                    <input type="text" name="section4_icontext[]" class="form-control mb-2" value="{{ $section4['value']->icontext[$i] }}" placeholder="Schoolership Facility">
                                    <input type="text" name="section4_description[]" class="form-control mb-2" value="{{ $section4['value']->description[$i] }}" placeholder="Description Items">
                                    <input type="file" name="section4_icon[]" class="form-control mb-2" value="">
                                </div>
                            @endfor
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mt-2">
                            <label class="form-label">Foto</label>
                            @if ($section4['image'] != null)
                                <br>
                                <div class="text-center">
                                    <img src="{{ '/images/home/'.$section4['image'] }}" alt="foto" class="img-fluid w-50 mb-2">
                                </div>
                            @endif
                            <input type="file" name="section4_image" class="form-control" value="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <button type="submit" class="btn btn-inverse-primary px-5 mt-2 mb-5">Save</button>
    </form>

    @include('sweetalert::alert')

@endsection