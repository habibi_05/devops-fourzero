@extends('cms.layouts.base')

@section('content')
    <div class="card">
        <div class="card-header d-flex py-3">
            <h4>Contact Email</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="table-contact" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th width="125" class="text-center">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    @include('sweetalert::alert')

@endsection

@section('sectionFooter')
    <script>
        $(function() {
            $("#table-contact").DataTable({
                processing: true,
                serverSide: true,
                ajax: "/contact-email/dataJson",
                columns: [
                    // { data: "id", name: "id" },
                    {
                        data: "indo_date",
                        name: "indo_date"
                    },
                    {
                        data: "first_name",
                        name: "first_name"
                    },
                    {
                        data: "last_name",
                        name: "last_name"
                    },
                    {
                        data: "email",
                        name: "email"
                    },
                    {
                        data: "action",
                        name: "action",
                        className: 'text-center'
                    },
                ],
            });

            $("#table-contact").on("click", ".btn-delete[data-remote]", function(e) {
                e.preventDefault();
                var url = $(this).data("remote");
                console.log(url);
                swal({
                        title: "Apakah yakin?",
                        text: "Mengapus data contact email !!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false,
                    },
                    function() {
                        console.log(url);
                        $.ajax({
                            url: url,
                            type: "DELETE",
                            dataType: "json",
                            data: {
                                method: "_DELETE",
                                submit: true,
                                _token: "{{ csrf_token() }}",
                            },
                        }).always(function(data) {
                            $("#table-contact").DataTable().draw(false);
                            swal("Sukses !!", "Menghapus data contact email.", "success");
                        });
                    }
                );
            });
        });
    </script>
@endsection
