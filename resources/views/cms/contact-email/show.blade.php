@extends('cms.layouts.base')

@section('content')
    <div class="row mt-4">
        <div class="col-md-8 mx-auto">
            <div class="card border-top border-0 border-4 border-primary" id="addedit">
                <div class="card-body p-5">
                    <div class="card-title d-flex align-items-center">
                        <h5 class="mb-0 text-primary">{{ $meta['title'] }}</h5>
                    </div>
                    <hr>
                    <div class="row g-3">
                        <div class="col-md-4">
                            <label class="form-label"><strong>First Name</strong></label>
                            <p class="m-0">{{ $data->first_name }}</p>
                        </div>
                        <div class="col-md-4">
                            <label class="form-label"><strong>Last Name</strong></label>
                            <p class="m-0">{{ $data->last_name }}</p>
                        </div>
                        <div class="col-md-4">
                            <label class="form-label"><strong>Email</strong></label>
                            <p class="m-0">{{ $data->email }}</p>
                        </div>
                        <div class="col-md-12">
                            <label class="form-label"><strong>Message</strong></label>
                            <p class="m-0">{{ $data->message }}</p>
                        </div>
                        <div class="col-12">
                            <a href="{{ url('contact-email') }}" class="btn btn-inverse-danger px-5 mt-2">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
