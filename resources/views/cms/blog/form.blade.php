@extends('cms.layouts.base')

@section('content')
    <div class="card border-top border-0 border-4 border-primary" id="addedit">
        <div class="card-body p-5">
            <div class="card-title d-flex align-items-center">
                <div><i class="bx bxs-book-add me-1 font-22 text-primary"></i></div>
                <h5 class="mb-0 text-primary">{{ $meta['title'] }}</h5>
            </div>
            <hr>
            @if (!$data->id)
                <form method="POST" class="row g-3" action="{{ route('blog.store') }}" enctype="multipart/form-data">
            @else
                <form method="POST" class="row g-3" action="{{ url('blog/' . $data->id) }}" enctype="multipart/form-data">
                    {{ method_field('PUT') }}
            @endif
            @csrf
            @include('cms.layouts.validation_error')
                <div class="col-md-6" id="colusername">
                    <label for="inputTitle" class="form-label">Title</label>
                    <input type="text" name="title" class="form-control" id="inputTitle" value="{{ $data->title }}" required>
                </div>
                <div class="col-md-6">
                    <label for="inputSubtitle" class="form-label">Subtitle</label>
                    <input type="text" name="subtitle" class="form-control" id="inputSubtitle" value="{{ $data->subtitle }}" required>
                </div>
                <div class="col-md-6">
                    <label for="inputKategori" class="form-label">Kategori</label>
                    <select class="form-select" name="kategori" required>
                        @foreach ($kategori as $item)
                            <option value="{{ strtolower($item->kategori) }}" {{ strtolower($item->kategori) == $data->kategori ? 'selected' : null }}>{{ ucwords($item->kategori) }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="inputTag" class="form-label">Tag</label>
                    <select name="tag[]" id="tag" class="multiple-select form-control" multiple="multiple">
                        @if ($data->tag != '' && $data->tag != null)
                            @php
                                $tags = explode(',', $data->tag);
                            @endphp
                            @foreach ($tags as $item)
                                <option value="{{ $item }}" selected>{{ $item }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-md-8">
                    <label for="inputIsi" class="form-label">Isi</label>
                    <textarea name="isi" class="my-editor form-control" id="my-editor" cols="30" rows="60">
                        {{ $data->isi }}
                    </textarea>
                </div>
                <div class="col-md-4">
                    <label for="inputFoto" class="form-label">Foto</label>
                    <input type="file" name="foto" class="form-control" id="inputFoto">
                    @if (!empty($data) && $data->foto != null && $data->foto != '')
                        <img src="/images/blog/{{ $data->foto }}" alt="foto" width="300">
                    @endif
                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-inverse-primary px-5">Save</button>
                    <a href="{{ url('course') }}" class="btn btn-inverse-danger px-5">Cancel</a>
                </div>
            </form>
        </div>
    </div>

    @include('sweetalert::alert')

@endsection
