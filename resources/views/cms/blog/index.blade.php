@extends('cms.layouts.base')

@section('content')
    <div class="card">
        <div class="card-header d-flex py-3">
            <h4>Blog</h4>
            <a href="{{ url('blog/create') }}" class="btn btn-inverse-primary ms-auto">Add</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="table-blog" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Foto</th>
                            <th>Title</th>
                            <th>Penulis</th>
                            <th>Kategori</th>
                            <th width="125" class="text-center">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    @include('sweetalert::alert')

@endsection

@section('sectionFooter')
    <script>
        $(function() {
            $("#table-blog").DataTable({
                processing: true,
                serverSide: true,
                ajax: "/blog/dataJson",
                columns: [
                    // { data: "id", name: "id" },
                    {
                        data: "foto",
                        render: function(data, type, row) {
                            return '<img src="/images/blog/'+data+'" width="100" class="img img-fluid" alt="gambar">';
                        },
                    },
                    {
                        data: "title",
                        name: "title"
                    },
                    {
                        data: "penulis",
                        name: "penulis"
                    },
                    {
                        data: "kategori",
                        name: "kategori"
                    },
                    {
                        data: "action",
                        name: "action",
                        className: 'text-center'
                    },
                ],
            });

            $("#table-blog").on("click", ".btn-delete[data-remote]", function(e) {
                e.preventDefault();
                var url = $(this).data("remote");
                console.log(url);
                swal({
                        title: "Apakah yakin?",
                        text: "Mengapus data Blog !!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false,
                    },
                    function() {
                        console.log(url);
                        $.ajax({
                            url: url,
                            type: "DELETE",
                            dataType: "json",
                            data: {
                                method: "_DELETE",
                                submit: true,
                                _token: "{{ csrf_token() }}",
                            },
                        }).always(function(data) {
                            $("#table-blog").DataTable().draw(false);
                            swal("Sukses !!", "Menghapus data Blog.", "success");
                        });
                    }
                );
            });
        });
    </script>
@endsection
