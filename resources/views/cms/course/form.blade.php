@extends('cms.layouts.base')

@section('content')
    <div class="card border-top border-0 border-4 border-primary" id="addedit">
        <div class="card-body p-5">
            <div class="card-title d-flex align-items-center">
                <div><i class="bx bxs-book-add me-1 font-22 text-primary"></i></div>
                <h5 class="mb-0 text-primary">{{ $meta['title'] }}</h5>
            </div>
            <hr>
            @if (!$data->id)
                <form method="POST" class="row g-3" action="{{ route('course.store') }}">
            @else
                <form method="POST" class="row g-3" action="{{ url('course/' . $data->id) }}">
                    {{ method_field('PUT') }}
            @endif
            @csrf
            @include('cms.layouts.validation_error')
                <div class="col-md-6" id="colusername">
                    <label for="inputTitle" class="form-label">Title</label>
                    <input type="text" name="title" class="form-control" id="inputTitle" value="{{ $data->title }}" required>
                </div>
                <div class="col-md-6">
                    <label for="inputLink" class="form-label">Link</label>
                    <input type="text" name="link" class="form-control" id="inputLink" value="{{ $data->link }}" required>
                </div>
                <div class="col-md-6">
                    <label for="inputStatus" class="form-label">Status</label>
                    <select id="inputStatus" name="status" class="form-select" required>
                        <option value="Aktif" {{ $data->status == 'Aktif' ? 'selected' : '' }}>Aktif</option>
                        <option value="Tidak Aktif" {{ $data->status == 'Tidak Aktif' ? 'selected' : '' }}>Tidak Aktif</option>
                    </select>
                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-inverse-primary px-5">Save</button>
                    <a href="{{ url('course') }}" class="btn btn-inverse-danger px-5">Cancel</a>
                </div>
            </form>
        </div>
    </div>

    @include('sweetalert::alert')

@endsection
