@extends('cms.layouts.base')

@section('content')
    <div class="card">
        <div class="card-header d-flex py-3">
            <h4>Course</h4>
            <a href="{{ url('course/create') }}" class="btn btn-inverse-primary ms-auto">Add</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="table-course" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Link</th>
                            <th width="125" class="text-center">Status</th>
                            <th width="125" class="text-center">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    @include('sweetalert::alert')

@endsection

@section('sectionFooter')
    <script>
        $(function() {
            $("#table-course").DataTable({
                processing: true,
                serverSide: true,
                ajax: "/course/dataJson",
                columns: [
                    // { data: "id", name: "id" },
                    {
                        data: "title",
                        name: "title"
                    },
                    {
                        data: "link",
                        name: "link"
                    },
                    {
                        data: "status",
                        render: function(data, type, row) {
                            if (data == "Aktif") {
                                return '<span class="btn btn-inverse-success">Aktif</span>';
                            } else {
                                return '<span class="btn btn-inverse-danger">Tidak Aktif</span>';
                            }
                        },
                        className: 'text-center',
                    },
                    {
                        data: "action",
                        name: "action",
                        className: 'text-center'
                    },
                ],
            });

            $("#table-course").on("click", ".btn-delete[data-remote]", function(e) {
                e.preventDefault();
                var url = $(this).data("remote");
                console.log(url);
                swal({
                        title: "Apakah yakin?",
                        text: "Mengapus data user !!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false,
                    },
                    function() {
                        console.log(url);
                        $.ajax({
                            url: url,
                            type: "DELETE",
                            dataType: "json",
                            data: {
                                method: "_DELETE",
                                submit: true,
                                _token: "{{ csrf_token() }}",
                            },
                        }).always(function(data) {
                            $("#table-course").DataTable().draw(false);
                            swal("Sukses !!", "Menghapus data user.", "success");
                        });
                    }
                );
            });
        });
    </script>
@endsection
