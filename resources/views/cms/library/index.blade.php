@extends('cms.layouts.base')

@section('content')
    <div class="card">
        <div class="card-header d-flex py-3">
            <h4>Library</h4>
            <a href="{{ url('library/create') }}" class="btn btn-inverse-primary ms-auto">Add</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="table-library" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Foto</th>
                            <th>Nama</th>
                            <th>File</th>
                            <th width="125" class="text-center">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    @include('sweetalert::alert')

@endsection

@section('sectionFooter')
    <script>
        $(function() {
            $("#table-library").DataTable({
                processing: true,
                serverSide: true,
                ajax: "/library/dataJson",
                columns: [
                    // { data: "id", name: "id" },
                    {
                        data: "foto",
                        render: function(data, type, row) {
                            return '<img src="/images/library/'+data+'" width="100" class="img img-fluid" alt="gambar">';
                        },
                    },
                    {
                        data: "nama",
                        name: "nama"
                    },
                    {
                        data: "file",
                        render: function(data, type, row) {
                            return '<a class="text-black" href="/pdf/'+data+'" target="_blank"><i class="bx bx-file text-danger"></i> Lihat Pdf</a>';
                        },
                    },
                    {
                        data: "action",
                        name: "action",
                        className: 'text-center'
                    },
                ],
            });

            $("#table-library").on("click", ".btn-delete[data-remote]", function(e) {
                e.preventDefault();
                var url = $(this).data("remote");
                console.log(url);
                swal({
                        title: "Apakah yakin?",
                        text: "Mengapus data library !!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false,
                    },
                    function() {
                        console.log(url);
                        $.ajax({
                            url: url,
                            type: "DELETE",
                            dataType: "json",
                            data: {
                                method: "_DELETE",
                                submit: true,
                                _token: "{{ csrf_token() }}",
                            },
                        }).always(function(data) {
                            $("#table-library").DataTable().draw(false);
                            swal("Sukses !!", "Menghapus data Library.", "success");
                        });
                    }
                );
            });
        });
    </script>
@endsection
