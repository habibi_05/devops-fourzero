@extends('cms.layouts.base')

@section('content')
    <div class="card border-top border-0 border-4 border-primary" id="addedit">
        <div class="card-body p-5">
            <div class="card-title d-flex align-items-center">
                <div><i class="bx bxs-book-add me-1 font-22 text-primary"></i></div>
                <h5 class="mb-0 text-primary">{{ $meta['title'] }}</h5>
            </div>
            <hr>
            @if (!$data->id)
                <form method="POST" class="row g-3" action="{{ route('library.store') }}" enctype="multipart/form-data">
            @else
                <form method="POST" class="row g-3" action="{{ url('library/' . $data->id) }}" enctype="multipart/form-data">
                    {{ method_field('PUT') }}
            @endif
            @csrf
            @include('cms.layouts.validation_error')
                <div class="col-12" id="colusername">
                    <label for="inputNama" class="form-label">Nama</label>
                    <input type="text" name="nama" class="form-control" id="inputNama" value="{{ $data->nama }}" required>
                </div>
                <div class="col-md-8">
                    <label for="inputDeskripsi" class="form-label">Deskripsi</label>
                    <textarea name="deskripsi" class="my-editor form-control" id="my-editor" cols="30" rows="60">
                        {{ $data->deskripsi }}
                    </textarea>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-12">
                            <label for="inputFoto" class="form-label">Foto</label>
                            <input type="file" name="foto" class="form-control" id="inputFoto" {{ $data->foto != null && $data->foto != '' ? null : 'required' }}>
                            @if ($data->foto != null && $data->foto != '')
                                <img src="/images/library/{{ $data->foto }}" alt="foto" width="300">
                            @endif
                        </div>
                        <div class="col-12 mt-3">
                            <label for="inputFile" class="form-label">file</label>
                            <input type="file" name="file" class="form-control" id="inputFile" {{ $data->file != null && $data->file != '' ? null : 'required' }}>
                            @if ($data->file != null && $data->file != '')
                                <br><a class="text-black" href="/pdf/{{ $data->file }}" target="_blank"><i class="bx bx-file text-danger"></i> Lihat File</a>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-inverse-primary px-5">Save</button>
                    <a href="{{ url('course') }}" class="btn btn-inverse-danger px-5">Cancel</a>
                </div>
            </form>
        </div>
    </div>

    @include('sweetalert::alert')

@endsection
