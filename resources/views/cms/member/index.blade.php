@extends('cms.layouts.base')

@section('content')
    <div class="card">
        <div class="card-header d-flex py-3">
            <h4>{{ $meta['title'] }}</h4>
            {{-- <a href="{{ url('user/create') }}" class="btn btn-inverse-primary ms-auto">Add</a> --}}
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="table-member" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th width="125" class="text-center">Status</th>
                            <th width="125" class="text-center">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    @include('sweetalert::alert')

@endsection

@section('sectionFooter')
    <script>
        $(function() {
            $("#table-member").DataTable({
                processing: true,
                serverSide: true,
                ajax: "/member/dataJson",
                columns: [
                    // { data: "id", name: "id" },
                    {
                        data: "first_name",
                        name: "first_name"
                    },
                    {
                        data: "last_name",
                        name: "last_name"
                    },
                    {
                        data: "email",
                        name: "email"
                    },
                    {
                        data: "status",
                        render: function(data, type, row) {
                            if (data == "Aktif") {
                                return '<span class="btn btn-inverse-success">Aktif</span>';
                            } else {
                                return '<span class="btn btn-inverse-danger">Tidak Aktif</span>';
                            }
                        },
                        className: 'text-center',
                    },
                    {
                        data: "action",
                        name: "action",
                        className: 'text-center'
                    },
                ],
            });

            $("#table-member").on("click", ".btn-delete[data-remote]", function(e) {
                e.preventDefault();
                var url = $(this).data("remote");
                console.log(url);
                swal({
                        title: "Apakah yakin?",
                        text: "Mengapus data user !!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false,
                    },
                    function() {
                        console.log(url);
                        $.ajax({
                            url: url,
                            type: "DELETE",
                            dataType: "json",
                            data: {
                                method: "_DELETE",
                                submit: true,
                                _token: "{{ csrf_token() }}",
                            },
                        }).always(function(data) {
                            $("#table-member").DataTable().draw(false);
                            swal("Sukses !!", "Menghapus data member.", "success");
                        });
                    }
                );
            });
        });
    </script>
@endsection
