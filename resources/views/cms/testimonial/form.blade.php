@extends('cms.layouts.base')

@section('content')
    <div class="card border-top border-0 border-4 border-primary" id="addedit">
        <div class="card-body p-5">
            <div class="card-title d-flex align-items-center">
                <h5 class="mb-0 text-primary">{{ $meta['title'] }}</h5>
            </div>
            <hr>
            @if (!$data->id)
                <form method="POST" class="row g-3" action="{{ route('testimonial.store') }}" enctype="multipart/form-data">
            @else
                <form method="POST" class="row g-3" action="{{ url('testimonial/' . $data->id) }}" enctype="multipart/form-data">
                    {{ method_field('PUT') }}
            @endif
                @csrf
                @include('cms.layouts.validation_error')
                <div class="col-md-8">
                    <div class="mb-3">
                        <label for="inputName" class="form-label">Name</label>
                        <input type="text" name="name" class="form-control" id="inputName" value="{{ $data->name }}" required>
                    </div>
                    <div class="mb-3">
                        <label for="inputPosition" class="form-label">Position</label>
                        <input type="text" name="position" class="form-control" id="inputPosition" value="{{ $data->position }}" required>
                    </div>
                    <div class="mb-3">
                        <label for="inputDescription" class="form-label">Description</label>
                        <textarea name="description" id="inputDescription" class="form-control" cols="30" rows="10" required>{{ $data->description }}</textarea>
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="inputFoto" class="form-label">Foto</label>
                    @if (!empty($data) && $data->foto != null && $data->foto != '')
                        <br>
                        <div class="text-center">
                            <img src="{{ url('images/testimonial/'.$data->foto) }}" alt="foto" class="img-fluid w-75 mb-3">
                        </div>
                    @endif
                    <input type="file" name="foto" class="form-control" id="inputFoto">
                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-inverse-primary px-5">Save</button>
                    <a href="{{ url('testimonial') }}" class="btn btn-inverse-danger px-5">Cancel</a>
                </div>
            </form>
        </div>
    </div>

    @include('sweetalert::alert')

@endsection
