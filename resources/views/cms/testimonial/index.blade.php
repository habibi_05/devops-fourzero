@extends('cms.layouts.base')

@section('content')
    <div class="card">
        <div class="card-header d-flex py-3">
            <h4>{{ $meta['title'] }}</h4>
            <a href="{{ route('testimonial.create') }}" class="btn btn-inverse-primary ms-auto">Add</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="table-testimonial" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th width="150">Foto</th>
                            <th width="300">Name</th>
                            <th width="150">Postion</th>
                            <th>Description</th>
                            <th width="125" class="text-center">Action</th>
                            {{-- <img src="" alt=""> --}}
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    @include('sweetalert::alert')

@endsection

@section('sectionFooter')
    <script>
        $(function() {
            let base = '{{ url('/') }}';
            $("#table-testimonial").DataTable({
                processing: true,
                serverSide: true,
                ajax: "/testimonial/dataJson",
                columns: [
                    // { data: "id", name: "id" },
                    {
                        data: "foto",
                        render: function(data, type, row) {
                            return '<img class="img-fluid" src="'+base+'/images/testimonial/'+data+'" >';
                        },
                        className: 'text-center',
                    },
                    {
                        data: "name",
                        name: "name"
                    },
                    {
                        data: "position",
                        name: "position"
                    },
                    {
                        data: "description",
                        name: "description"
                    },
                    {
                        data: "action",
                        name: "action",
                        className: 'text-center'
                    },
                ],
            });

            $("#table-testimonial").on("click", ".btn-delete[data-remote]", function(e) {
                e.preventDefault();
                var url = $(this).data("remote");
                console.log(url);
                swal({
                        title: "Apakah yakin?",
                        text: "Mengapus data testimonial !!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false,
                    },
                    function() {
                        console.log(url);
                        $.ajax({
                            url: url,
                            type: "DELETE",
                            dataType: "json",
                            data: {
                                method: "_DELETE",
                                submit: true,
                                _token: "{{ csrf_token() }}",
                            },
                        }).always(function(data) {
                            $("#table-testimonial").DataTable().draw(false);
                            swal("Sukses !!", "Menghapus data testimonial.", "success");
                        });
                    }
                );
            });
        });
    </script>
@endsection
