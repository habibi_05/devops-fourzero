@if ($errors->any())
<style>.alert .text-white p{margin: 0;}</style>
<div class="alert alert-danger border-0 bg-danger alert-dismissible fade show request-any">
    @foreach ($errors->all() as $error)
        <div class="text-white">{{ $error }}</div>
    @endforeach
    <button type="button" class="btn-close text-white" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

@if (Session::get('errorss'))
<style>.alert .text-white p{margin: 0;}</style>
<div class="alert alert-danger border-0 bg-danger alert-dismissible fade show request-custom">
    @foreach (Session::get('errorss') as $message)
        @if (is_array($message))
            @foreach ($message as $subMsg)
                <div class="text-white">{{ $subMsg }}</div>
            @endforeach
        @else
            <div class="text-white">{{ $message }}</div>
        @endif
    @endforeach
    <button type="button" class="btn-close text-white" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif