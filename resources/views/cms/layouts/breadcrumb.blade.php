<div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
    <div class="breadcrumb-title pe-3">{{ $meta['title'] }}</div>
    <div class="ps-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0 p-0">
                @foreach ($meta['breadcrumb'] as $key => $value)
                    @if (!empty($value))
                        <li class="breadcrumb-item"><a href="{{ url($value) }}">{{ $key }}</a></li>
                    @else
                        <li class="breadcrumb-item active" aria-current="page">{{ $key }}</li>
                    @endif
                @endforeach
            </ol>
        </nav>
    </div>
</div>
