<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    
    <!--favicon-->
    <link rel="icon" href="{{ url('dashboard/images/favicon-32x32.png') }}" type="image/png" />
    <title>Dashboard Fourzero - {{ $meta['title'] }}</title>
    <!--plugins-->
    <link href="{{ url('dashboard/plugins/simplebar/css/simplebar.css') }}" rel="stylesheet" />
    <link href="{{ url('dashboard/plugins/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet" />
    <link href="{{ url('dashboard/plugins/metismenu/css/metisMenu.min.css') }}" rel="stylesheet" />
    <!-- loader-->
    <link href="{{ url('dashboard/css/pace.min.css') }}" rel="stylesheet" />
    <script src="{{ url('dashboard/js/pace.min.js') }}"></script>
    <!-- Bootstrap CSS -->
    <link href="{{ url('dashboard/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('dashboard/css/bootstrap-extended.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
    <link href="{{ url('dashboard/css/app.css') }}" rel="stylesheet">
    <link href="{{ url('dashboard/css/icons.css') }}" rel="stylesheet">
    <link href="{{ url('dashboard/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('dashboard/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
	<link href="{{ url('dashboard/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet" />

    {{-- plugin --}}
    @isset($plugins['css'])
        @foreach ($plugins['css'] as $value)
            <link rel="stylesheet" href="{{ url($value) }}">
        @endforeach
    @endisset

</head>

<body>
    <!--wrapper-->
    <div class="wrapper">
        <!--sidebar wrapper -->
        @include('cms.layouts.sidebar')
        <!--end sidebar wrapper -->
        <!--start header -->
        @include('cms.layouts.header')
        <!--end header -->
        <!--start page wrapper -->
        <div class="page-wrapper">
            <div class="page-content">
                @if ($meta['title'] != "Dashboard")
                    @include('cms.layouts.breadcrumb')
                @endif

				@yield('content')
            </div>
        </div>
        <!--end page wrapper -->
        @include('cms.layouts.footer')
    </div>
    <!--end wrapper-->
    <!-- Bootstrap JS -->
    <script src="{{ url('dashboard/js/bootstrap.bundle.min.js') }}"></script>
    <!--plugins-->
    <script src="{{ url('dashboard/js/jquery.min.js') }}"></script>
    <script src="{{ url('dashboard/plugins/simplebar/js/simplebar.min.js') }}"></script>
    <script src="{{ url('dashboard/plugins/metismenu/js/metisMenu.min.js') }}"></script>
    <script src="{{ url('dashboard/plugins/perfect-scrollbar/js/perfect-scrollbar.js') }}"></script>
    <script src="{{ url('dashboard/plugins/select2/js/select2.min.js') }}"></script>
    <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
    <!--app JS-->
    <script src="{{ url('dashboard/js/app.js') }}"></script>

	{{-- plugin --}}
	@isset($plugins['js'])
		@foreach ($plugins['js'] as $value)
			<script src="{{ url($value) }}"></script>
		@endforeach
	@endisset

    {{-- section footer --}}
    @yield('sectionFooter')
</body>

</html>
