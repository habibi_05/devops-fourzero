<div class="sidebar-wrapper" data-simplebar="true">
    <div class="sidebar-header">
        {{-- <div>
            <img src="{{ url('dashboard/images/logo-fourzero.png') }}" class="logo-icon" style="width: 25px" alt="logo icon">
        </div> --}}
        <div>
            <h4 class="logo-text">Fourzero</h4>
        </div>
        <div class="toggle-icon ms-auto"><i class='bx bx-arrow-to-left'></i>
        </div>
    </div>
    <!--navigation-->
    <ul class="metismenu" id="menu">
        <li class="menu-label">User Management</li>
        <li>
            <a href="{{ url('user') }}">
                <div class="parent-icon"><i class='bx bx-user'></i></div>
                <div class="menu-title">User</div>
            </a>
        </li>
        <li>
            <a href="{{ url('member') }}">
                <div class="parent-icon"><i class='bx bx-group'></i></div>
                <div class="menu-title">Member</div>
            </a>
        </li>
        <li class="menu-label">Page Management</li>
        <li>
            <a href="{{ url('homepage') }}">
                <div class="parent-icon"><i class='bx bx-home-circle'></i></div>
                <div class="menu-title">Homepage</div>
            </a>
        </li>
        <li>
            <a href="{{ url('contact') }}">
                <div class="parent-icon"><i class='bx bx-phone'></i></div>
                <div class="menu-title">Contact Us</div>
            </a>
        </li>
        <li>
            <a href="{{ url('testimonial') }}">
                <div class="parent-icon"><i class='bx bx-message-detail'></i></div>
                <div class="menu-title">Testimonial</div>
            </a>
        </li>
        <li>
            <a href="{{ url('contact-email') }}">
                <div class="parent-icon"><i class='bx bx-mail-send'></i></div>
                <div class="menu-title">Contact Email</div>
            </a>
        </li>
        <li class="menu-label">Content Management</li>
        <li>
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='bx bx-message-rounded-edit'></i>
                </div>
                <div class="menu-title">Blog</div>
            </a>
            <ul>
                <li>
                    <a href="{{ url('kategori_blog') }}"><i class="bx bx-right-arrow-alt"></i>Kategori</a>
                </li>
                <li>
                    <a href="{{ url('blog') }}"><i class="bx bx-right-arrow-alt"></i>Blog</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="{{ url('course') }}">
                <div class="parent-icon"><i class='bx bx-book-add'></i></div>
                <div class="menu-title">Course</div>
            </a>
        </li>
        <li>
            <a href="{{ url('library') }}">
                <div class="parent-icon"><i class='bx bx-blanket'></i></div>
                <div class="menu-title">Library</div>
            </a>
        </li>
        <li>
            <a href="{{ url('event') }}">
                <div class="parent-icon"><i class='bx bx-calendar-event'></i></div>
                <div class="menu-title">Event</div>
            </a>
        </li>
    </ul>
    <!--end navigation-->
</div>