@extends('cms.layouts.base')

@section('content')
    <div class="card border-top border-0 border-4 border-primary" id="addedit">
        <div class="card-body p-5">
            <div class="card-title d-flex align-items-center">
                <div><i class="bx bxs-user me-1 font-22 text-primary"></i></div>
                <h5 class="mb-0 text-primary">{{ $meta['title'] }}</h5>
            </div>
            <hr>
            @if (!$data->id)
                <form method="POST" class="row g-3" action="{{ route('user.store') }}">
            @else
                <form method="POST" class="row g-3" action="{{ url('user/' . $data->id) }}">
                    {{ method_field('PUT') }}
            @endif
            @csrf
            @include('cms.layouts.validation_error')
                <div class="col-md-6" id="colusername">
                    <label for="inputUsername" class="form-label">Username</label>
                    <input type="text" name="username" class="form-control" id="inputUsername" value="{{ $data->username }}" required>
                </div>
                <div class="col-md-6">
                    <label for="inputLastName" class="form-label">Fullname</label>
                    <input type="text" name="nama_lengkap" class="form-control" id="inputLastName" value="{{ $data->nama_lengkap }}" required>
                </div>
                <div class="col-md-6" id="colemail">
                    <label for="inputEmail" class="form-label">Email</label>
                    <input type="email" name="email" class="form-control" id="inputEmail" value="{{ $data->email }}" required>
                </div>
                <div class="col-md-6">
                    <label for="inputPassword" class="form-label">Password</label>
                    <input type="password" name="password" class="form-control" id="inputPassword" {{ !$data->id ? 'required' : '' }} >
                </div>
                <div class="col-md-6">
                    <label for="inputStatus" class="form-label">Status</label>
                    <select id="inputStatus" name="status" class="form-select" required>
                        <option value="Aktif" {{ $data->status == 'Aktif' ? 'selected' : '' }}>Active</option>
                        <option value="Tidak Aktif" {{ $data->status == 'Tidak Aktif' ? 'selected' : '' }}>Inactive</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="inputRole" class="form-label">Role</label>
                    <select id="inputRole" name="role" class="form-select" required>
                        <option value="admin" {{ $data->role == 'admin' ? 'selected' : '' }}>Admin</option>
                        <option value="content" {{ $data->role == 'content' ? 'selected' : '' }}>Content</option>
                    </select>
                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-inverse-primary px-5">Save</button>
                    <a href="{{ url('user') }}" class="btn btn-inverse-danger px-5">Cancel</a>
                </div>
            </form>
        </div>
    </div>

    @include('sweetalert::alert')

@endsection
