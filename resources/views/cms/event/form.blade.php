@extends('cms.layouts.base')

@section('content')
    <div class="card border-top border-0 border-4 border-primary" id="addedit">
        <div class="card-body p-5">
            <div class="card-title d-flex align-items-center">
                <div><i class="bx bxs-book-add me-1 font-22 text-primary"></i></div>
                <h5 class="mb-0 text-primary">{{ $meta['title'] }}</h5>
            </div>
            <hr>
            @if (!$data->id)
                <form method="POST" class="row g-3" action="{{ route('event.store') }}" enctype="multipart/form-data">
            @else
                <form method="POST" class="row g-3" action="{{ url('event/' . $data->id) }}" enctype="multipart/form-data">
                    {{ method_field('PUT') }}
            @endif
            @csrf
            @include('cms.layouts.validation_error')
                <div class="col-md-6" id="colJudul">
                    <label for="inputJudul" class="form-label">Judul</label>
                    <input type="text" name="title" class="form-control" id="inputJudul" value="{{ $data->title }}" required>
                </div>
                <div class="col-md-6" id="colLink">
                    <label for="inputLink" class="form-label">Link</label>
                    <input type="text" name="link" class="form-control" id="inputLink" value="{{ $data->link }}">
                </div>
                <div class="col-md-6" id="colHarga">
                    <label for="inputHarga" class="form-label">Harga</label><small class="text-secondary">  (* angka saja)</small>
                    <input type="text" name="harga" class="form-control" id="inputHarga" value="{{ $data->harga }}" required>
                </div>
                <div class="col-md-6" id="colDurasi">
                    <label for="inputDurasi" class="form-label">Durasi</label>
                    <input type="text" name="duration" class="form-control" id="inputDurasi" value="{{ $data->duration }}" required>
                </div>
                <div class="col-md-8">
                    <label for="inputDeskripsi" class="form-label">Deskripsi</label>
                    <textarea name="deskripsi" class="my-editor form-control" id="my-editor" cols="30" rows="60">
                        {{ $data->deskripsi }}
                    </textarea>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-12">
                            <label for="inputTanggal" class="form-label">Tanggal</label>
                            <input type="date" name="tanggal" class="form-control" id="inputTanggal" value="{{ $data->tanggal }}" required>
                        </div>
                        <div class="col-12 mt-3">
                            <label for="inputFoto" class="form-label">Foto</label>
                            <input type="file" name="foto" class="form-control" id="inputFoto" {{ $data->foto != null && $data->foto != '' ? null : 'required' }}>
                            @if ($data->foto != null && $data->foto != '')
                                <img src="/images/event/{{ $data->foto }}" alt="foto" width="300">
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-inverse-primary px-5">Save</button>
                    <a href="{{ url('course') }}" class="btn btn-inverse-danger px-5">Cancel</a>
                </div>
            </form>
        </div>
    </div>

    @include('sweetalert::alert')

@endsection
