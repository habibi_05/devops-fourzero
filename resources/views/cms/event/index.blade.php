@extends('cms.layouts.base')

@section('content')
    <div class="card">
        <div class="card-header d-flex py-3">
            <h4>Event</h4>
            <a href="{{ url('event/create') }}" class="btn btn-inverse-primary ms-auto">Add</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="table-event" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Foto</th>
                            <th>Judul</th>
                            <th>Tanggal</th>
                            <th>Harga</th>
                            <th>Durasi</th>
                            <th width="125" class="text-center">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    @include('sweetalert::alert')

@endsection

@section('sectionFooter')
    <script>
        $(function() {
            $("#table-event").DataTable({
                processing: true,
                serverSide: true,
                ajax: "/event/dataJson",
                columns: [
                    // { data: "id", name: "id" },
                    {
                        data: "foto",
                        render: function(data, type, row) {
                            return '<img src="/images/event/'+data+'" width="100" class="img img-fluid" alt="gambar">';
                        },
                    },
                    {
                        data: "title",
                        name: "title"
                    },
                    {
                        data: "tanggal",
                        render: function(data, type, row) {
                            return formattingTanggal(data);
                        },
                    },
                    {
                        data: "harga",
                        render: function(data, type, row) {
                            return 'Rp. ' + formatRupiah(data);
                        },
                    },
                    {
                        data: "duration",
                        name: "duration"
                    },
                    {
                        data: "action",
                        name: "action",
                        className: 'text-center'
                    },
                ],
            });

            $("#table-event").on("click", ".btn-delete[data-remote]", function(e) {
                e.preventDefault();
                var url = $(this).data("remote");
                console.log(url);
                swal({
                        title: "Apakah yakin?",
                        text: "Mengapus data event !!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false,
                    },
                    function() {
                        console.log(url);
                        $.ajax({
                            url: url,
                            type: "DELETE",
                            dataType: "json",
                            data: {
                                method: "_DELETE",
                                submit: true,
                                _token: "{{ csrf_token() }}",
                            },
                        }).always(function(data) {
                            $("#table-event").DataTable().draw(false);
                            swal("Sukses !!", "Menghapus data Event.", "success");
                        });
                    }
                );
            });
        });

        function formattingTanggal(tanggal) {
			var pecah 	= tanggal.split(' ');
			var tgl		= pecah[0].split('-');

			var bulanArray = {
				'01'  : "Januari",
				'02'  : "Febuari",
				'03'  : "Maret",
				'04'  : "April",
				'05'  : "Mei",
				'06'  : "Juni",
				'07'  : "Juli",
				'08'  : "Agustus",
				'09'  : "September",
				'10'  : "Oktober",
				'11'  : "November",
				'12'  : "Desember"
			};

			var bulanConvert = bulanArray[tgl[1]];

			return tgl[2]+' '+bulanConvert+' '+tgl[0] ;
		}


        function formatRupiah(uang) {  
			var convertUang = uang.toString(),
				sisa 		= convertUang.length % 3,
				rupiah 		= convertUang.substr(0, sisa),
				ribuan		= convertUang.substr(sisa).match(/\d{3}/g);

			if (ribuan) {
				seperator = sisa ? '.' : '';
				rupiah += seperator	+ ribuan.join('.');
			}

			return rupiah;
		}

    </script>
@endsection
