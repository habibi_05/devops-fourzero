@extends('cms.layouts.base')

@section('content')
    <form method="POST" action="{{ route('contact.store') }}">
        @csrf
        @include('cms.layouts.validation_error')
        <div class="card radius-10 border-top border-0 border-3 border-primary">
            <div class="card-body">
                <div class="card-title">
                    <h5 class="mb-0 text-primary">Contact</h5>
                    <hr>
                </div>
                <div class="row g-3">
                    <div class="row mt-2">
                        <div class="col-md-6">
                            <label class="form-label">Phone</label><strong class="text-danger"> *</strong>
                            <input type="text" name="phone" class="form-control" value="{{ $data['value']->phone }}" required>
                        </div>
                        <div class="col-md-6">
                            <label class="form-label">Email</label><strong class="text-danger"> *</strong>
                            <input type="text" name="email" class="form-control" value="{{ $data['value']->email }}" required>
                        </div>
                    </div>
                    <div class="mt-2">
                        <label class="form-label">address</label><strong class="text-danger"> *</strong>
                        <textarea name="address" class="form-control" required>{{ $data['value']->address }}</textarea>
                    </div>
                </div>
            </div>
        </div>
        
        <button type="submit" class="btn btn-inverse-primary px-5 mt-2 mb-5">Save</button>
    </form>

    @include('sweetalert::alert')

@endsection