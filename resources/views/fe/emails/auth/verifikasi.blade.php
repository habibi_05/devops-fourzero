@component('mail::message')
# Verifikasi Register Akun

Silahkan Klik Link dibawah untuk Verifikasi <br>
<a href="{{$link}}" target="_blank">Klik Disini</a>

Terima Kasih,<br>
{{ env('MAIL_FROM_NAME') }}
@endcomponent
