@component('mail::message')

First Name: {{$data['firstname']}}<br>
Last Name: {{$data['lastname']}}<br>
Email: {{$data['email']}}<br>
Message:<br>
{{$data['message']}}<br>

Thank You.<br>
{{ env('MAIL_FROM_NAME') }}
@endcomponent
