@extends('fe.layouts.base')

@section('content')
    <!-- contact area start -->
    <div class="contact-area pd-top-110 pd-bottom-120">
        <div class="container">
            <div class="section-title text-center">
                <h2 class="title">Get in touch</h2>
            </div>
            <div class="row">
                <div class="col-lg-7 col-md-6 align-self-center mx-auto">
                    @include('fe.layouts.alert')
                    <form class="contact-form-inner mt-5 mt-md-0" method="POST" action="{{ url('contact/send') }}">
                        @csrf
                        <div class="row custom-gutters-20">
                            <div class="col-lg-6">
                                <label class="single-input-inner style-bg-border">
                                    <input type="text" placeholder="First Name" name="firstname">
                                </label>
                            </div>
                            <div class="col-lg-6">
                                <label class="single-input-inner style-bg-border">
                                    <input type="text" placeholder="Last Name" name="lastname">
                                </label>
                            </div>
                            <div class="col-12">
                                <label class="single-input-inner style-bg-border">
                                    <input type="text" placeholder="Email" name="email">
                                </label>
                            </div>
                            <div class="col-12">
                                <label class="single-input-inner style-bg-border">
                                    <textarea placeholder="Message" name="message"></textarea>
                                </label>
                            </div>
                            <div class="col-12 text-center">
                                <button class="btn btn-base">Send Message</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- contact area end-->

@endsection
