@extends('fe.layouts.base')

@section('content')
    @php $class = ['color-base', 'color-pink', 'color-blue']; @endphp

    <!-- banner start -->
    <div class="banner-area banner-area-1 mb-5 mb-lg-0" style="background-image: url({{ url('fe/img/banner/01.png') }});">
        <div class="banner-bg"></div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-9 order-lg-12">
                    <div class="thumb mb-4 mb-lg-0">
                        @if ($header['image'])
                            <img src="{{ url('images/home/'.$header['image']) }}" alt="img">
                        @endif
                    </div>
                </div>
                <div class="col-lg-7 order-lg-1 align-self-center">
                    <div class="banner-inner text-center text-lg-left">
                        <!-- <p class="b-animate-1 sub-title sub-title-after">Welcome To Siksha</p> -->
                        <h1 class="b-animate-2 title">{{ $header['value']->title }} <span>{{ $header['value']->subtitle }}</span></h1>
                        <p class="content b-animate-3">{{ $header['value']->description }}</p>
                        <div class="btn-wrap b-animate-4">
                            <a class="btn btn-base-m mr-3" href="{{ $header['value']->buttonlink }}" target="_blank">{{ $header['value']->buttontext }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- banner end -->

    <!-- intro start -->
    <div class="intro-area intro-one-area mg-top--82">
        <div class="container">
            <div class="row">
                @if ($header['icon'])
                    @foreach ($header['icon'] as $key => $value)
                        <div class="col-xl-2 col-lg-3 col-md-4">
                            <div class="single-intro-inner text-center">
                                <div class="thumb">
                                    <img src="{{ url('images/home/'.$value) }}" alt="img">
                                </div>
                                <div class="details">
                                    <h6 class="title {{ $class[$key] }}">{{ $header['value']->icontext[$key] }}</h6>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    <!-- intro end -->

    <!-- about area start -->
    @if ($section1['status'] == 1)
        <div class="about-area home-1-about bg-relative pd-top-90 pd-bottom-120"
            style="background-image: url({{ url('fe/img/bg/1.png') }});">
            <div class="container">
                <div class="about-area-inner">
                    <div class="row">
                        <div class="col-lg-6 col-md-11">
                            <div class="about-thumb-wrap about-left-thumb">
                                <img src="{{ url('images/home/'.$section1['image']) }}" alt="img">
                            </div>
                        </div>
                        <div class="col-lg-6 align-self-center">
                            <div class="about-inner-wrap mt-2 pt-4 pt-lg-0">
                                <div class="section-title mb-0">
                                    <h2 class="title">{{ $section1['value']->title }} <span>{{ $section1['value']->subtitle }}</span></h2>
                                    <p class="content mb-4">{{ $section1['value']->description }}</p>
                                    @if ($section1['value']->icontext)
                                        @foreach ($section1['value']->icontext as $key => $value)
                                            <p class="{{ $class[$key] }} mb-3"><img class="mr-2" src="{{ $key % 2 ? url('fe/img/icon/5.png') : url('fe/img/icon/4.png') }}" alt="img"> {{ $value }}</p>
                                        @endforeach
                                    @endif
                                    <a class="btn btn-base-m" href="{{ $section1['value']->buttonlink }}">{{ $section1['value']->buttontext }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!-- about area end -->

    <!-- event area start -->
    @if (count($event))
        <div class="event-area pd-top-120">
            <div class="container">
                <div class="section-title text-center">
                    <h2 class="title">Upcoming <span>Events</span></h2>
                </div>
                @foreach ($event as $item)
                    <div class="single-event-inner media">
                        <div class="media-left">
                            <img width="213" src="{{ url('images/event/'.$item->foto) }}" alt="img">
                        </div>
                        <div class="media-body align-self-center">
                            <a class="right-arrow" href="{{ url('event/detail/'.$item->id) }}"><i class="fa fa-mail-forward"></i></a>
                            <p class="date">{{ tanggalIndo($item->tanggal) }} | {{ $item->duration }}</p>
                            <h4><a href="{{ url('event/detail/'.$item->id) }}">{{ ucfirst(strLimit($item->title, 75)) }}</a></h4>
                            <p class="content">{!! ucfirst(strLimit($item->deskripsi, 200)) !!}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
    <!-- event area end  -->

    <!-- category start -->
    @if ($section2['status'] == 1)
        <div class="category-area">
            <div class="container">
                <div class="section-title text-center">
                    <h2 class="title">{{ $section2['value']->title }} <span>{{ $section2['value']->subtitle }}</span></h2>
                </div>
                <div class="category-slider owl-carousel">
                    @if ($section2['icon'])
                        @foreach ($section2['icon'] as $key => $value)
                            <div class="item">
                                <div class="single-intro-inner style-category media">
                                    <div class="thumb media-left">
                                        <img src="{{ url('images/home/'.$value) }}" alt="img">
                                    </div>
                                    <div class="details media-body align-self-center">
                                        <h6 class="title color-base"><a href="{{ $section2['value']->iconlink[$key] }}">{{ $section2['value']->icontext[$key] }}</a></h6>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    @endif
    <!-- category end -->

    <!-- video area start -->
    @if ($section3['status'] == 1)
        <div class="video-area bg-relative pd-top-90 pd-bottom-115">
            <div class="container">
                <div class="about-area-inner">
                    <div class="row">
                        <div class="col-lg-6 col-md-11 order-lg-12">
                            <div class="video-thumb">
                                <img src="{{ url('images/home/'.$section3['image']) }}" alt="img">
                                {{-- <a class="video-play-btn" href="https://www.youtube.com/embed/Wimkqo8gDZ0"
                                    data-effect="mfp-zoom-in"><i class="fa fa-play"></i></a> --}}
                            </div>
                        </div>
                        <div class="col-lg-6 order-lg-1 align-self-center">
                            <div class="about-inner-wrap mt-2 pt-4 pt-lg-0">
                                <div class="section-title mb-0">
                                    <h2 class="title">{{ $section3['value']->title }} <span>{{ $section3['value']->subtitle }}</span></h2>
                                    <p class="content mb-4">{{ $section3['value']->description }}</p>
                                </div>
                                <div class="row">
                                    @if ($section3['value']->totalitems)
                                        @foreach ($section3['value']->totalitems as $key => $value)
                                            <div class="col-lg-4 col-md-4">
                                                <div class="counter-list-inner text-center">
                                                    <div class="details">
                                                        <h2><span class="counter">{{ $value }}</span>+</h2>
                                                        <p>{{ $section3['value']->titleitems[$key] }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!-- video area end -->

    <!-- testimonial area start -->
    <div class="testimonial-area pd-top-120 pd-bottom-115 bg-light">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-5 col-lg-6 col-md-11">
                    <div class="section-title text-center">
                        <h2 class="title">What They Are Talking <span>Testimonial</span></h2>
                    </div>
                </div>
            </div>
            <div class="testimonial-slider slider-control-dots owl-carousel">
                @if ($testimonial)
                    @foreach ($testimonial as $value)
                        <div class="item">
                            <div class="single-testimonial-inner">
                                <img class="side-icon" src="{{ url('fe/img/icon/7.png') }}" alt="img-quote">
                                <p>{{ $value->description }}</p>
                                <div class="media">
                                    <div class="media-left">
                                        <img src="{{ url('images/testimonial/'.$value->foto) }}" alt="{{ $value->name }}">
                                    </div>
                                    <div class="media-body align-self-center">
                                        <h6>{{ $value->name }}</h6>
                                        <span>{{ $value->position }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    <!-- testimonial area end -->

    <!-- about area start -->
    @if ($section4['status'] == 1)
        <div class="about-area pd-top-120 bg-relative">
            <div class="container">
                <div class="about-area-inner">
                    <div class="row">
                        <div class="col-lg-6 col-md-10 text-lg-right">
                            <div class="thumb">
                                <img src="{{ url('images/home/'.$section4['image']) }}" alt="img">
                            </div>
                        </div>
                        <div class="col-lg-6 align-self-center">
                            <div class="about-inner-wrap pl-lg-5 pt-4 pt-lg-0">
                                <div class="section-title mb-0">
                                    <h2 class="title">{{ $section4['value']->title }} <span>{{ $section4['value']->subtitle }}</span></h2>
                                    @if ($section4['icon'])
                                        @foreach ($section4['icon'] as $key => $value)
                                            <div class="media single-list-inner mt-4">
                                                <div class="media-left mr-3">
                                                    <img src="{{ url('images/home/'.$value) }}" alt="img">
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="color-base">{{ $section4['value']->icontext[$key] }}</h4>
                                                    <p class="mb-0">{{ $section4['value']->description[$key] }}</p>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!-- about area end -->
@endsection
