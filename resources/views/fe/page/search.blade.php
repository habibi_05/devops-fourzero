@extends('fe.layouts.base')

@section('content')
    <div class="main-team-area pd-top-120 pd-bottom-120">
        <div class="container">
            @if (count($data) > 0)
                @foreach ($data as $value)
                    @if ($value->type == "course" )
                        <div class="single-event-inner media">
                            <div class="media-body align-self-center">
                                <a class="right-arrow" target="_blank" href="{{ $value->subtitle }}"><i class="fa fa-mail-forward"></i></a>
                                <h4><a target="_blank" href="{{ $value->subtitle }}">{{ ucfirst(strLimit($value->title, 75)) }}</a></h4>
                                <h6 class="color-base">{{ ucfirst($value->type) }}</h6>
                            </div>
                        </div>
                    @else
                        <div class="single-event-inner media">
                            <div class="media-left">
                                <img width="213" src="{{ url('images/'.$value->type.'/'.$value->foto) }}" alt="img">
                            </div>
                            <div class="media-body align-self-center">
                                <a class="right-arrow" href="{{ url($value->type.'/detail/'.$value->id) }}"><i class="fa fa-mail-forward"></i></a>
                                <h4><a href="{{ url($value->type.'/detail/'.$value->id) }}">{{ ucfirst(strLimit($value->title, 75)) }}</a></h4>
                                <p class="content">{!! ucfirst(strLimit($value->subtitle, 200)) !!}</p>
                                <p class="date"><a href="{{ url($value->type) }}" class="font-weight-bold color-base">{{ ucfirst($value->type) }}</a> {{ tanggalIndo($value->created_at) }}</p>
                            </div>
                        </div>
                    @endif
                @endforeach
            @else
                <div class="col-12 text-center">
                    <h3 class="font-weight-bold">Data Tidak Tersedia</h3>
                </div>
            @endif
            <div id="loadmore"></div>
            @if (count($data) == 5)
                <div class="text-center">
                    <button class="btn btn-base-m" id="btn-loadmore" type="button" data-page="search" data-keywoard="{{ Request::get('q') }}" data-offset="5" data-limit="5">Loadmore</button>
                </div>
            @endif
        </div>
    </div>
@endsection
