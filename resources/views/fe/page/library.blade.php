@extends('fe.layouts.base')

@section('content')
    <!-- team area start -->
    <div class="team-area pd-top-120 pd-bottom-90">
        <div class="container">
            <div class="row justify-content-center" id="loadmore">
                @if (count($dataLibrary['data']) > 0)
                    @foreach ($dataLibrary['data'] as $item)
                        <div class="col-lg-4 col-md-6">
                            <div class="single-team-inner text-center">
                                <div class="thumb">
                                    <img src="{{ url('images/library/'. $item->foto) }}" alt="cover">
                                </div>
                                <div class="details">
                                    <h4><a href="{{ url('library/detail/'. $item->id) }}">{{ ucwords($item->nama) }}</a></h4>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="col-12 text-center">
                        <h3 class="font-weight-bold">Data Tidak Tersedia</h3>
                    </div>
                @endif
            </div>
            <div class="row justify-content-center">
                @if ($dataLibrary['dataCount'] > 6)
                    <div class="col-12 text-center">
                        <button class="btn btn-base-m" id="btn-loadmore" type="button" data-page="library" data-offset="6" data-limit="3">Loadmore</button>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <!-- team area end -->
@endsection
