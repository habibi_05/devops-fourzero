@extends('fe.layouts.base')

@section('content')
    <div class="main-blog-area blog-details-page-content pd-top-120 pd-bottom-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8 col-12">
                    <div class="single-blog-inner mb-0">
                        <div class="thumb">
                            <img src="/images/event/{{ $data->foto }}" alt="img">
                        </div>
                        <div class="details">
                            <div class="blog-meta">
                                <ul>
                                    {{--  <li><i class="fa fa-user"></i> Mortin</li>  --}}
                                    {{--  <li><i class="fa fa-clock-o"></i> April 19, 2020</li>  --}}
                                    {{--  <li><i class="fa fa-folder-open"></i> Marketing</li>  --}}
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="blog-content-inner">
                        <h4>Deskripsi Event</h4>
                        {!! $data->deskripsi !!}
                    </div>
                </div>
                <!-- sidebar -->
                <div class="col-lg-4 col-12">
                    <div class="td-sidebar">
                        <div class="widget widget-video-inner">
                            <div class="header text-center">Event Details</div>
                            <div class="details">
                                <ul>
                                    <li><span>Harga:</span> {{ rupiah($data->harga) }}</li>
                                    <li><span>Durasi :</span> {{ $data->duration }}</li>
                                    <li><span>Tanggal:</span> {{ tanggalIndo2($data->tanggal) }}</li>
                                    @if ($data->link != null && $data->link != '')
                                        <li><span>Link :</span> <a href="{{ $data->link }}" target="_blank"> <u>Kunjungi Link</u></a></li>
                                    @endif
                                </ul>
                                <a class="btn btn-base-m w-100" href="#">Ikuti Event</a>
                            </div>
                        </div>                
                    </div>
                </div>
                <!-- /.sidebar -->
            </div>
        </div>
    </div>
@endsection
