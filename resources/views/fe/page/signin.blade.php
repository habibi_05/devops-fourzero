@extends('fe.layouts.base')

@section('content')
    <!-- signup-page-Start -->
    <div class="signup-page-area mt-5 pd-top-120 pd-bottom-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-7">
                    @include('fe.layouts.alert')
                    <form class="signin-inner" action="{{ route('signinProcess')}}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-12">
                                <div class="single-input-inner style-bg-border">
                                    <input type="email" name="email" placeholder="Email">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="single-input-inner style-bg-border">
                                    <input type="password" name="password" placeholder="Password">
                                </div>
                            </div>
                            <div class="col-12 mb-4">
                                <button class="btn btn-base-m w-100">Sign In</button>
                            </div>
                            <div class="col-12">
                                <span>By creating an account</span>
                                <a href="{{ url('signup') }}"><strong>Signup</strong></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- signup-page-end -->
@endsection
