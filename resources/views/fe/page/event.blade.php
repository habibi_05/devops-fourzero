@extends('fe.layouts.base')

@section('content')
    <div class="main-team-area pd-top-120 pd-bottom-120">
        <div class="container">
            @if (count($dataEvent['data']) > 0)
                @foreach ($dataEvent['data'] as $item)
                {{--  @if (file_exists('images/event/'.$item->foto))  --}}
                    <div class="single-event-inner media">
                        <div class="media-left">
                            <img width="213" src="/images/event/{{ $item->foto }}" alt="img">
                        </div>
                        <div class="media-body align-self-center">
                            <a class="right-arrow" href="/event/detail/{{ $item->id }}"><i class="fa fa-mail-forward"></i></a>
                            <p class="date">{{ $item->duration }}</p>
                            <h4><a href="/event/detail/{{ $item->id }}">{{ ucfirst(strLimit($item->title, 75)) }}</a></h4>
                            <p class="content">{!! ucfirst(strLimit($item->deskripsi, 200)) !!}</p>
                            {{--  <h6>Robin Meany</h6>
                            <p class="designation">Senior manager</p>  --}}
                        </div>
                    </div>
                {{--  @else
                    <div class="single-event-inner media">
                        <div class="media-body align-self-center">
                            <p class="date">{{ $item->duration }}</p>
                            <h4>{{ ucfirst(strLimit($item->title, 50)) }}</h4>
                        </div>
                    </div>
                @endif  --}}
                @endforeach
            @else
                <div class="col-12 text-center">
                    <h3 class="font-weight-bold">Data Tidak Tersedia</h3>
                </div>
            @endif
            <div id="loadmore"></div>
            @if ($dataEvent['dataCount'] > 5)
                <div class="text-center">
                    <button class="btn btn-base-m" id="btn-loadmore" type="button" data-page="event" data-offset="5" data-limit="5">Loadmore</button>
                </div>
            @endif
        </div>
    </div>
@endsection
