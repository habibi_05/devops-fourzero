@extends('fe.layouts.base')

@section('content')
<div class="main-blog-area blog-details-page-content pd-top-120 pd-bottom-120">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-12">
                <div class="single-blog-inner mb-0">
                    <h2>{{ ucfirst($data->title) }}</h2>
                    <div class="thumb">
                        <img src="/images/blog/{{ $data->foto }}" alt="img">
                    </div>
                    <div class="details">
                        <div class="blog-meta">
                            <ul>
                                <li><i class="fa fa-user"></i> {{ ucwords($data->penulis) }}</li>
                                <li><i class="fa fa-clock-o"></i> {{ tanggalIndo($data->created_at) }}</li>
                                <li><i class="fa fa-folder-open"></i> {{ ucwords($data->kategori) }}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="blog-content-inner">
                    {!! $data->isi !!}
                    <div class="tag-and-sharea-area">
                        <div class="row">
                            <div class="col-md-6 align-self-center">
                                <div class="tags">
                                    <strong>Tags : </strong>
                                    @if ($data->tag != null)
                                        @php
                                            $tags = explode(',', $data->tag)
                                        @endphp
                                        @foreach ($tags as $item)
                                            <a href="#" class="tag">{{$item}}</a>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            {{--  <div class="col-md-6 align-self-center text-md-right">
                                <strong>Share : </strong>
                                <ul class="social-media-2 style-black mt-3 mt-md-0 d-inline-block">
                                    <li>
                                        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </div>  --}}
                        </div>
                    </div>
                    {{--  <form class="blog-comment-form">
                        <div class="section-title style-small mb-4 mt-5">
                            <h4 class="mb-0">Leave a Comments</h4>
                        </div>
                        <div class="row custom-gutters-20">
                            <div class="col-lg-6">
                                <label class="single-input-inner style-border">
                                    <input type="text" placeholder="Name">
                                </label>
                            </div>
                            <div class="col-lg-6">
                                <label class="single-input-inner style-border">
                                    <input type="text" placeholder="Email">
                                </label>
                            </div>
                            <div class="col-12">
                                <label class="single-input-inner style-border">
                                    <textarea placeholder="Message"></textarea>
                                </label>
                            </div>
                            <div class="col-12">
                                <button class="btn btn-base-m">Post Comment</button>
                            </div>
                        </div>
                    </form>  --}}
                </div>
            </div>
            <!-- sidebar -->
            <div class="col-lg-4 col-12">
                <div class="td-sidebar">
                    <div class="widget widget-recent-post">
                        <h4 class="widget-title">Recent Post</h4>
                        <ul>
                            @foreach ($blog as $item)
                                <li>
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="/blog/detail/{{ $item->id }}">
                                                <img width="100" src="/images/blog/{{ $item->foto }}" alt="blog">
                                            </a>
                                        </div>
                                        <div class="media-body align-self-center">
                                            <h5 class="title"><a href="/blog/detail/{{ $item->id }}">{{ ucfirst(strLimit($item->title, 25)) }}</a></h5>
                                            <div class="post-info"><i class="fa fa-calendar-times-o"></i> {{ tanggalIndo2($item->created_at) }}</div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /.sidebar -->
        </div>
    </div>
</div>
@endsection
