@extends('fe.layouts.base')

@section('content')
    <!--blog-area start-->
    <div class="blog-area pd-top-120 pd-bottom-120">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    @if (count($dataBlog['data']) > 0)
                        @foreach ($dataBlog['data'] as $item)
                            <div class="single-blog-inner">
                                <div class="thumb">
                                    <img src="/images/blog/{{ $item->foto }}" alt="img">
                                </div>
                                <div class="details">
                                    <div class="blog-meta">
                                        <ul>
                                            <li><i class="fa fa-user"></i> {{ ucwords($item->penulis) }}</li>
                                            <li><i class="fa fa-clock-o"></i> {{ tanggalIndo($item->created_at) }}</li>
                                            <li><i class="fa fa-folder-open"></i> {{ ucwords($item->kategori) }}</li>
                                        </ul>
                                    </div>
                                    <h4><a href="/blog/detail/{{ $item->id }}">{{ ucfirst(strLimit($item->title, 25)) }}</a></h4>
                                    <p>{{ strLimit($item->subtitle, 70) }}</p>
                                    <a class="btn btn-base-m" href="/blog/detail/{{ $item->id }}">Read More +</a>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="col-12 text-center">
                            <h3 class="font-weight-bold">Data Tidak Tersedia</h3>
                        </div>
                    @endif
                    <div id="loadmore"></div>
                    @if ($dataBlog['dataCount'] > 3)
                        <div class="text-center">
                            <button class="btn btn-base-m" id="btn-loadmore" type="button" data-page="blog" data-offset="3" data-limit="3">Loadmore</button>
                        </div>
                    @endif
                </div>
                <!-- sidebar -->
                <div class="col-lg-4 col-12">
                    <div class="td-sidebar">                
                        <div class="widget widget-recent-post">                            
                            <h4 class="widget-title">Recent Post</h4>
                            <ul>
                                <li>
                                    <div class="media">
                                        <div class="media-left">
                                            <img src="{{ url('fe/img/blog/w1.png') }}" alt="blog">
                                        </div>
                                        <div class="media-body align-self-center">
                                            <h5 class="title"><a href="single-blog.html">World’s most famous app developers</a></h5>
                                            <div class="post-info"><i class="fa fa-calendar-times-o"></i> 25 Jan, 2021</div>                             
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media">
                                        <div class="media-left">
                                            <img src="{{ url('fe/img/blog/w2.png') }}" alt="blog">
                                        </div>
                                        <div class="media-body align-self-center">
                                            <h5 class="title"><a href="single-blog.html">Custom Platform for an Audit Insurance</a></h5>
                                            <div class="post-info"><i class="fa fa-calendar-times-o"></i> 25 Jan, 2021</div>                             
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media">
                                        <div class="media-left">
                                            <img src="{{ url('fe/img/blog/w3.png') }}" alt="blog">
                                        </div>
                                        <div class="media-body align-self-center">
                                            <h5 class="title"><a href="single-blog.html">Famous app and Designer</a></h5>
                                            <div class="post-info"><i class="fa fa-calendar-times-o"></i> 25 Jan, 2021</div>                                 
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /.sidebar -->
            </div>
        </div>
    </div>
    <!--blog-area end-->
@endsection