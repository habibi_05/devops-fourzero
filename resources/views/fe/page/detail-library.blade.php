@extends('fe.layouts.base')

@section('content')
    <!-- single blog page -->
    <div class="main-blog-area pd-top-120 pd-bottom-90">
        <div class="container">
            <div class="team-details-page">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="thumb mb-4 mb-0">
                            <img src="/images/library/{{ $data->foto }}" alt="img">
                        </div>
                    </div>
                    <div class="col-lg-8 align-self-center">
                        <div class="details">
                            <h3 class="mb-1">{{ $data->nama }}</h3>
                            {!! $data->deskripsi !!}

                            <a class="btn btn-base-m" href="/library/download/{{ $data->file }}">Download Library</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="course-area pd-top-100">
                <h4 class="mb-4">Library</h4>
                {{--  <h4 class="mb-4">Course by : artincorsese</h4>  --}}
                <div class="row justify-content-center" id="loadmore">
                    @if (count($library) > 0)
                        @foreach ($library as $item)
                            <div class="col-lg-4 col-md-6">
                                <div class="single-team-inner text-center">
                                    <div class="thumb">
                                        <img src="{{ url('images/library/'.$item->foto) }}" alt="cover">
                                    </div>
                                    <div class="details">
                                        <h4><a href="{{ url('library/detail/'.$item->id) }}">{{ ucwords($item->nama) }}</a></h4>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- single blog page end -->
@endsection
