@extends('fe.layouts.base')

@section('content')
    <!-- signup-page-Start -->
    <div class="signup-page-area mt-5 pd-top-120 pd-bottom-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-7">
                    <div class="signin-inner">
                        <div class="row">
                            <div class="col-12">
                                <div class="style-bg-border">
                                    <h3 class="mb-0">{{ $message }}</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- signup-page-end -->
@endsection
