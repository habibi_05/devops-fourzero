@if (Session::get('errors'))
    <div class="alert alert-danger" role="alert">
        @foreach (Session::get('errors') as $message)
            @if (is_array($message))
                @foreach ($message as $subMsg)
                    {{ $subMsg }} <br>
                @endforeach
            @else
                {{ $message }}
            @endif
        @endforeach
    </div>
@endif

@if (Session::get('success'))
    <div class="alert alert-success" role="alert">
        @foreach (Session::get('success') as $message)
            @if (is_array($message))
                @foreach ($message as $subMsg)
                    {{ $subMsg }} <br>
                @endforeach
            @else
                {{ $message }}
            @endif
        @endforeach
    </div>
@endif