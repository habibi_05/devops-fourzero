<!-- footer area start -->
<footer class="footer-area pd-top-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="widget widget_about pr-xl-4">
                    <div class="thumb">
                        <img src="{{ url('fe/img/logo.png') }}" alt="img">
                    </div>
                    <div class="details">
                        <p class="mb-3">Siksha to bring changes online based learning by doing course Fusce varius, dolor tempor interdum tris.</p>
                        <ul class="social-media-2 mt-3">
                            <li>
                                <a href="#">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="widget widget_nav_menu">
                    <h4 class="widget-title">Menu</h4>
                    <ul>
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <li><a href="{{ url('event') }}">Event</a></li>
                        <li><a href="{{ url('blog') }}">Blog</a></li>
                        <li><a href="{{ url('contact') }}">Contact Us</a></li>
                        <li><a href="{{ url('library') }}">Library</a></li>
                    </ul>
                </div>
            </div>
            {{-- <div class="col-lg-2 col-md-6">
                <div class="widget widget_nav_menu">
                    <h4 class="widget-title">Category</h4>
                    <ul>
                        <li><a href="course.html">ALL Course</a></li>
                        <li><a href="course.html">Marketing</a></li>
                        <li><a href="course.html">Art</a></li>
                        <li><a href="course.html">Designing</a></li>
                        <li><a href="course.html">Data Analist</a></li>
                    </ul>
                </div>
            </div> --}}
            <div class="col-lg-4 col-md-6">
                <div class="widget widget_contact pl-lg-3">
                    <h4 class="widget-title">Contact Us</h4>
                    @php $contact = getContact() @endphp
                    <ul class="details">
                        <li><i class="fa fa-phone"></i> {{ $contact['value']->phone }}</li>
                        <li><i class="fa fa-envelope"></i> {{ $contact['value']->email }}</li>
                        <li><i class="fa fa-map-marker"></i> {{ $contact['value']->address }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12 align-self-center">
                    <p>Copyright © 2021 Fourzero.id. All Right reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer area end -->