<div class="navbar-area">
    <nav class="navbar navbar-area-1 navbar-area navbar-expand-lg">
        <div class="container nav-container">
            <div class="responsive-mobile-menu">
                <button class="menu toggle-btn d-block d-lg-none" data-target="#edumint_main_menu" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="icon-left"></span>
                    <span class="icon-right"></span>
                </button>
            </div>
            <div class="logo">
                <a href="{{ url('') }}"><img src="{{ url('fe/img/logo.png') }}" alt="img"></a>
            </div>
            <div class="nav-right-part nav-right-part-mobile">
                <a class="search-bar-btn" href="#"><i class="fa fa-search"></i></a>
            </div>
            <div class="collapse navbar-collapse" id="edumint_main_menu">
                <ul class="navbar-nav menu-open text-right">
                    <li><a href="{{ url('') }}">Home</a></li>
                    @php
                        $course = getCourse();
                    @endphp
                    @if ($course)
                        <li class="menu-item-has-children">
                            <a href="#">Course</a>
                            <ul class="sub-menu">
                                @foreach ($course as $value)
                                    <li><a href="{{ $value->link }}" target="_blank">{{ $value->title }}</a></li>
                                @endforeach
                            </ul>
                        </li>
                    @else
                        <li class="menu-item-has-children"><a href="#">Course</a></li>
                    @endif
                    <li class="menu-item-has-children">
                        <a href="#">Product</a>
                        <ul class="sub-menu">
                            <li><a href="https://journal.fourzero.id" target="_blank">Journal</a></li>
                            <li><a href="{{ url('event') }}">Event</a></li>
                        </ul>
                    </li>
                    <li><a href="{{ url('blog') }}">Blog</a></li>
                    <li><a href="{{ url('contact') }}">Contact Us</a></li>
                    <li><a href="{{ url('library') }}">Library</a></li>
                </ul>
            </div>
            <div class="nav-right-part nav-right-part-desktop">
                <a class="search-bar-btn" href="#"><i class="fa fa-search"></i></a>
                @if (getGuardFe())
                    <div class="dropdown dropdown-custom-hover d-inline-block ml-3">
                        <button class="btn btn-base-m dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-lg fa-user mr-2"></i> {{ getGuardFe()->first_name }}
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="{{ url('signout') }}">Sign out</a>
                        </div>
                    </div>
                @else
                    <div class="log-reg-btn d-inline-block ml-3">
                        <a class="btn {{ (isset($meta['pageTitle']) && ($meta['pageTitle'] == 'Signin' || $meta['pageTitle'] != 'Signup')) || empty($meta['pageTitle']) ? 'btn-base-m' : '' }}"
                            href="{{ url('signin') }}">Sign In</a>
                        <a class="btn {{ isset($meta['pageTitle']) && $meta['pageTitle'] == 'Signup' ? 'btn-base-m' : '' }}"
                            href="{{ url('signup') }}">Sign up</a>
                    </div>
                @endif
            </div>
        </div>
    </nav>
</div>
