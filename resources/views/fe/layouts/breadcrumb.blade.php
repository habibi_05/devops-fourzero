<!-- page title start -->
<div class="page-title-area bg-sky">
    <div class="container">
        <div class="row">
            <div class="col-xl-7 col-lg-8">
                <div class="breadcrumb-inner">
                    <h2 class="page-title">{{ $meta['pageTitle'] }}</h2>
                    <ul class="page-list">
                        <li><a href="{{ url('') }}">Home</a></li>
                        <li>{{ $meta['pageTitle'] }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- page title end -->