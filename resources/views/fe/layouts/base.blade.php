<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{ $meta['title'] }}</title>
    <link rel=icon href="{{ url('fe/img/favicon.png') }}" sizes="20x20" type="image/png">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="{{ url('fe/css/vendor.css') }}">
    <link rel="stylesheet" href="{{ url('fe/css/style.css') }}">
    <link rel="stylesheet" href="{{ url('fe/css/responsive.css') }}">

</head>
<body>

    <!-- preloader area start -->
    <div class="preloader" id="preloader">
        <div class="preloader-inner">
            <div class="spinner">
                <div class="dot1"></div>
                <div class="dot2"></div>
            </div>
        </div>
    </div>
    <!-- preloader area end -->

    <!-- search popup start-->
    <div class="td-search-popup" id="td-search-popup">
        <form action="search" class="search-form">
            <div class="form-group">
                <input type="text" class="form-control" autofocus name="q" placeholder="Search.....">
            </div>
            <button type="submit" class="submit-btn"><i class="fa fa-search"></i></button>
        </form>
    </div>
    <!-- search popup end-->
    <div class="body-overlay" id="body-overlay"></div>

    <!-- navbar start -->
    @include('fe.layouts.navbar')
    <!-- navbar end -->

    @if ( isset($meta['pageTitle']) && $meta['pageTitle'] != 'Signin' && $meta['pageTitle'] != 'Signup' )
        @include('fe.layouts.breadcrumb')
    @endif
    
    @yield('content')

    @include('fe.layouts.footer')

    <!-- back to top area start -->
    <div class="back-to-top">
        <span class="back-top"><i class="fa fa-angle-up"></i></span>
    </div>
    <!-- back to top area end -->

    <!-- all plugins here -->
    <script src="{{ url('fe/js/vendor.js') }}"></script>
    <!-- main js  -->
    <script src="{{ url('fe/js/main.js') }}"></script>
    <script src="{{ url('fe/js/app.js') }}"></script>
</body>
</html>